import React from 'react';
import { Layout, Row, Card, Col, Avatar, Divider } from 'antd';
import LayoutHeaderFooter from '../../components/LayoutDefault/HeaderFooter/HeaderFooter';
import './style.css';

class AboutUs extends React.Component {
  render() {
    return (
      <LayoutHeaderFooter>
        <Layout className="aboutus">
        <img className="mPicture" style={{ backgroundSize: 'cover', width: '100%' }} src="https://images3.alphacoders.com/875/thumb-1920-87577.jpg" alt="About us"></img>
        <div className={'mText1'}>
            <Row>
                <Col offset={10} span={6}><h3 style={{ color: 'white' }}>Về Chúng tôi </h3></Col>
            </Row>
            <Row style={{ width: '100%', color: 'white' }}>
                <Col span={7}>
                    <Card className="mCard" title="" bordered={false} style={{ width: '100%', height: 300, background: 'transparent', color: 'white' }}>
                        <p style={{ fontSize: 25 }}>Sứ mệnh của Uber Tutor</p>
                        <Divider></Divider>
                        <p style={{ fontSize: 20, fontStyle: 'italic' }}>Uber Tutor được tạo ra nhằm mục đích kết nối người học và người dạy một cách dễ dàng hơn, để từ đó có thể phát triển nền giáo dục lên một tầm cao mới, bên cạnh đó củng tao điều kiện việc làm cho các giáo viên.</p>
                    </Card>
                </Col>
                <Col span={7} offset={1}>
                    <Card className="mCard" title="" bordered={false} style={{ width: '100%', height: 300, background: 'transparent', color: 'white' }}>
                        <p style={{ fontSize: 25 }}>Mục tiêu phát triển</p>
                        <Divider></Divider>
                        <p style={{ fontSize: 20, fontStyle: 'italic' }}>Uber Tutor hướng đến việc đẩy mạnh dịch vụ học trực tuyến đang phát triển mạnh như hiện nay và tối ưu hoá các dịch vụ kết nối giữa người với người.</p>
                    </Card>
                </Col>
                <Col span={7} offset={1}>
                    <Card className="mCard" title="" bordered={false} style={{ width: '100%', height: 300, background: 'transparent', color: 'white' }}>
                        <p style={{ fontSize: 25 }}>Liên hệ</p>
                        <Divider></Divider>
                        <p style={{ fontSize: 20, fontStyle: 'italic' }}>Uber Tutor luôn có dịch vụ hỗ trợ 24/7. Nếu có bất cứ thắc mắc, xin vui lòng goi ngay cho đường đây nóng hoặc gửi E-mail cho chúng tôi đễ được hỗ trợ một cách tốt nhất.</p>
                    </Card>
                </Col>
            </Row>
            <Row>
                <Col span={23}><Divider></Divider></Col>
            </Row>

            <Col offset={9}><h3 style={{ color: 'white' }}>Đội ngũ phát triển</h3></Col>
            <Row style={{ textAlign: 'left' }}>
                <Col span={6} style={{ display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
                    <Avatar size={200} src="https://scontent.fsgn5-6.fna.fbcdn.net/v/t1.0-9/55788421_836882009980928_8512432323755507712_n.jpg?_nc_cat=106&_nc_ohc=RGKQBQI22FsAQkb0bGp4qR3r6LQxmT6O-TRSQL9bw098uc6AuJSTKfz0w&_nc_ht=scontent.fsgn5-6.fna&oh=1363a9b6506b1f8ac634dc22676f809c&oe=5E6E39AC"></Avatar>
                    <h3 style={{ color: 'white' }}>{'Trương Lê Việt Danh'}</h3>
                    <h5 style={{ color: 'white', fontStyle: 'italic' }}>{'Developer'}</h5>
                </Col>
                <Col span={6} offset={2} style={{ display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
                    <Avatar size={200} src="https://scontent.fsgn5-6.fna.fbcdn.net/v/t1.0-9/71875491_1389058384590495_2257585714453544960_n.jpg?_nc_cat=109&_nc_ohc=4HEBgxBmJi4AQlxTXPZSqfs05kyWx3E9AVztZKLtyVvqdi-jG2GWPLPPA&_nc_ht=scontent.fsgn5-6.fna&oh=1b081541bc16a6012f18b47a5be1bc49&oe=5E81753B"></Avatar>
                    <h3 style={{ color: 'white' }}>{'Phạm Thiên Bảo'}</h3>
                    <h5 style={{ color: 'white', fontStyle: 'italic' }}>{'Developer'}</h5>
                </Col>
                <Col span={6} offset={2} style={{ display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
                    <Avatar size={200} src="https://scontent.fsgn5-5.fna.fbcdn.net/v/t1.0-9/78377773_1154376398285297_2895087830457909248_n.jpg?_nc_cat=100&_nc_ohc=ZqAYwqnaTWAAQmOXTBWOIJREQocRHzsLhbV9tzjSSEaPwY-2cEoGnQ2OA&_nc_ht=scontent.fsgn5-5.fna&oh=79cd82f0c95f77161fd681110c4391e0&oe=5E81863F"></Avatar>
                    <h3 style={{ color: 'white' }}>{'Vũ Tuấn Anh'}</h3>
                    <h5 style={{ color: 'white', fontStyle: 'italic' }}>{'Developer'}</h5>
                </Col>
            </Row>

        </div>
        </Layout>

      </LayoutHeaderFooter>
    );
  }
}
export default AboutUs;
