import React from 'react';
import LayoutDefault from '../LayoutDefault';

const HeaderFooter = (props) => {
  const { children } = props;

  return (
    <LayoutDefault>
      <div style={{ padding: '24px' }} className='fill-content'>{children}</div>
    </LayoutDefault>
  );
};

export default HeaderFooter;
