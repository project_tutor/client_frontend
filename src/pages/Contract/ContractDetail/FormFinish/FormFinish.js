import React from 'react';
import { Form, Input, Rate, Button, notification } from 'antd';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

const STUDNET_FINISH_CONTRACT = gql`
mutation StudentFinishContract($contractID: ID!, $feedback: FeedbackInput){
  studentFinishContract(contractID: $contractID, feedback: $feedback)
}
`;
const { TextArea } = Input;

function FormFinish(props) {
  const { form, contractID, refetchDataContract, setVisibaleModal } = props;
  const { getFieldDecorator } = form;
  const [studentFinishContract] = useMutation(STUDNET_FINISH_CONTRACT);

  const handleSubmit = (e) => {
    e.preventDefault();
    form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        studentFinishContract({
          variables: {
            contractID,
            feedback: values,
          },
        }).then(() => {
          refetchDataContract();
          setVisibaleModal(false);
          notification.success({
            message: 'Bạn đã kết thúc hợp đồng thành công!',
          });
        });
      }
    });
  };

  return (
    <Form onSubmit={handleSubmit} className="login-form">
      <Form.Item label="Đánh giá">
        {getFieldDecorator('rate', {
          initialValue: 3,
        })(<Rate />)}
      </Form.Item>
      <Form.Item label="Bình luận">
        {getFieldDecorator('content', {
        })(
          <TextArea rows={4} />,
        )}
      </Form.Item>
      <Button type="primary" htmlType="submit" className="login-form-button">
        Xác nhận
      </Button>
    </Form>
  );
}

const WrapperFormFinish = Form.create({ name: 'FormFinish' })(FormFinish);

export default WrapperFormFinish;
