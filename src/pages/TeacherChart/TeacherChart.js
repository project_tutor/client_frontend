import React from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import { Spin, Card, Select } from 'antd';
import { withApollo } from 'react-apollo';

import { gql } from 'apollo-boost';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import HeaderFooterSilder from '../../components/LayoutDefault/HeaderFooterSilder/HeaderFooterSilder';

const GET_MY_CONTRACTS = gql`
  query myContracts {
  myContracts {
    _id
    feeHourly
    timeStudy
    state
    finishAt
  }
}
`;

class TeacherChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      myContracts: [],
      data: [],
      loading: false,
      dataYear: [],
      type: 'month',
    };

    this.GetContractData = this.GetContractData.bind(this);
    this.handleChangeSelect = this.handleChangeSelect.bind(this);
  }

  componentDidMount() {
    const { loading } = this.state;
    if (!loading) {
      this.setState({
        loading: !loading,
      }, () => {
        const data = [];
        const now = new Date();
        for (let i = 1; i <= now.getMonth() + 1; i++) {
          data.push({
            name: `${i < 10 ? `0${i}` : i}/${now.getFullYear()}`,
            money: 0,
          });
        }
        this.setState({ data }, () => {
          this.GetContractData();
        });
      });
    }
  }

  GetContractData() {
    const { client } = this.props;
    client.query({
      query: GET_MY_CONTRACTS,
    }).then((res) => {
      console.log((res.data.myContracts));
      this.setState({
        myContracts: res.data.myContracts,
      }, () => {
        this.GetIncomeData();
      });
    }).catch((err) => {
      console.log(err);
    });
  }

  GetIncomeData() {
    const { myContracts, data, dataYear } = this.state;
    const tempData = data.slice(0);
    const tempDataYear = dataYear.slice(0);
    console.log(tempData);
    myContracts.forEach((el) => {
      if (el.state === 'FINISH') {
        const date = new Date(+el.finishAt);
        tempData[date.getMonth()].money += parseInt(el.feeHourly, 0) * el.timeStudy;
        const index = this.CheckIsExistedData(date.getFullYear().toString(), tempDataYear);
        if (index !== -1) {
          tempDataYear[index].money += parseInt(el.feeHourly, 0) * el.timeStudy;
        } else {
          tempDataYear.push({ name: date.getFullYear().toString(), money: parseInt(el.feeHourly, 0) * el.timeStudy });
        }
      }
    });
    this.setState({ data: tempData, loading: false, dataYear: tempDataYear });
  }

  CheckIsExistedData(name, data) {
    for (let i = 0; i < data.length; i++) {
      if (data[i].name === name) {
        return i;
      }
    }
    return -1;
  }


  handleChangeSelect(value) {
    this.setState({ type: value });
  }


  render() {
    const { data, loading, dataYear, type } = this.state;
    const { user } = this.props;
    if (user.role === 'STUDENT') {
      return <Redirect to='/' />;
    }

    const mselect = (
      <div>
        <span>Thống kê theo: </span>
        <Select defaultValue="month" style={{ width: 120, marginLeft: 20 }} onChange={this.handleChangeSelect}>
          <Select.Option value="month">Tháng</Select.Option>
          <Select.Option value="year">Năm</Select.Option>
        </Select>
      </div>
    );

    return (<div>
      <HeaderFooterSilder >
        <Card title="Thống kê doanh thu" bordered={false} extra={mselect}>
          {loading ? <Spin /> : <LineChart width={1080} height={600} data={type === 'month' ? data : dataYear}
            margin={{ top: 5, right: 30, left: 20, bottom: 15 }}>
            <CartesianGrid strokeDasharray="5 5" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend verticalAlign="top" height={36} />
            <Line type="monotone" dataKey="money" stroke="#82ca9d" dot={{ stroke: 'red', strokeWidth: 2 }} />
          </LineChart>}
        </Card>
      </HeaderFooterSilder>
    </div>);
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps, null)(withApollo(TeacherChart));
