import React from 'react';
import { gql } from 'apollo-boost';
import { Layout, Row, Col, Avatar, Rate, Icon, Divider, Tag, Timeline, Comment, Tooltip, Button, Spin } from 'antd';
import { withApollo } from 'react-apollo';
import moment from 'moment';

const url_backend = process.env.REACT_APP_URL_BACKEND_API ? 'https://tutor-client-backend.herokuapp.com' : 'http://localhost:3001';

const FEEDBACK = gql`query getFeedbacksByPostID($input: ID!) {
    getFeedbacksByPostID(postID: $input){
      rate
      content
      createAt
      student {
          _id
          fullname
          imageURL
      }
    }
  }`;

class DetailInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      feedBack: [],
      loading: false,
    };
    this.handleCreateContact = this.handleCreateContact.bind(this);
  }

  componentWillMount() {
    const { id } = this.props;
    this.setState({ loading: true });
    this.props.client.query({
      query: FEEDBACK,
      variables: {
        input: id,
      },
    }).then((res) => {
      this.setState({ feedBack: res.data.getFeedbacksByPostID, loading: false });
    });
  }

  handleCreateContact() {
    this.props.handleCloseDrawer();
    this.props.handleOpenModal(this.props.id);
  }

  render() {
    const { feedBack, loading } = this.state;
    const { title, teacher, hourlyRate, overview, skill, rate, education, employee_history } = this.props;
    const { fullname, location, phone, imageURL } = teacher;
    return (
            <Layout className="Detail-profile">
                <Row style={{ background: 'white', overflow: 'hidden' }}>
                    <Col span={8} >
                        <Row style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                            <Avatar src={url_backend + imageURL} size={150}></Avatar>
                            <h1 style={{ marginTop: 5, color: 'green' }}>{fullname}</h1>
                        </Row>
                        <Row className="stars">
                            <Rate disabled defaultValue={rate} />
                        </Row>
                        <Divider orientation="left">Liên hệ</Divider>
                        <Row style={{ }}>
                            <div>
                                <Icon type="home" />
                                <div style={{ marginLeft: 10, padding: 'unset', display: 'inline' }} >{`${location.address}, ${location.city}`} </div>
                            </div>
                            <div>
                                <Icon type="phone" />
                                <p style={{ marginLeft: 10, display: 'inline' }} >{phone}</p>
                            </div>
                            <div>
                                <Icon type="dollar" />
                                <p style={{ marginLeft: 10, display: 'inline' }} >{`${hourlyRate}$/hrs`}</p>
                            </div>
                        </Row>
                        <Divider orientation="left">Môn học</Divider>
                        <Row>
                            {skill.map((skills) => (skills.isAproved ? <Tag key={skills._id} style={{ marginBottom: 10 }} color="cyan">{skills.name}</Tag> : null))}
                        </Row>

                    </Col>
                    <Col span={16}>
                        <Timeline>
                            <Timeline.Item>
                                <p style={{ fontSize: 16, color: 'red' }}>Giới thiệu</p>
                                <h3 style={{ fontStyle: 'bold' }}>{title}</h3>
                                <p>{overview}</p>
                            </Timeline.Item>
                            <Timeline.Item>
                                <p style={{ fontSize: 16, color: 'red' }}>Học vấn</p>
                                {education.map((el, index) => (<div key={index} >
                                    <h5 style={{ fontWeight: 'bold' }}>{el.name}</h5>
                                    <h5 style={{ fontStyle: 'italic' }}>{`${el.timeStart} - ${el.timeEnd}`}</h5>
                                    <p >{`${el.name} - ${el.description} - ${el.degree}`}</p>
                                </div>))}

                            </Timeline.Item>
                            <Timeline.Item>
                                <p style={{ fontSize: 16, color: 'red' }}>Kinh nghiệm làm việc</p>
                                {employee_history.map((el, index) => (<div key={index}>
                                    <h5 style={{ fontWeight: 'bold' }}>{el.title}</h5>
                                    <h5 style={{ fontStyle: 'italic' }}>{`${el.timeStart} - ${el.timeEnd}`}</h5>
                                    <p >{`${el.workPlace} - ${el.description}`}</p>
                                </div>))}
                            </Timeline.Item>
                            <Timeline.Item></Timeline.Item>
                        </Timeline>
                    </Col>
                </Row>
                <Divider orientation="left">Đánh giá</Divider>
                {loading && <Spin></Spin>}
                {!loading && feedBack.map((el, index) => (
                    <Row key={index} style={{}}>
                        <Comment
                            author={<p style={{ color: 'green' }}>{el.student.fullname}</p>}
                            avatar={ <Avatar src={el.student.imageURL} alt={el.student.fullname} /> }
                            content={
                                <div>
                                    <p>{el.content}</p>
                                    <Rate style={{ alignSelf: 'center' }} disabled defaultValue={el.rate} />
                                </div>
                            }
                            datetime={
                                <Tooltip title={moment(+el.createAt).format('YYYY-MM-DD HH:mm:ss')}>
                                    <span>{moment().fromNow()}</span>
                                </Tooltip>
                            }
                            >
                        </Comment>
                    </Row>
                ))}
                <Button className="btn" type="primary" icon="plus" onClick={this.handleCreateContact}
                    >Đăng ký</Button>
            </Layout>);
  }
}

export default withApollo(DetailInfo);
