import React, { useState } from 'react';
import {
  Input,
  Card, Form, Row, Col, InputNumber, Button, notification, DatePicker,
} from 'antd';
import { useMutation } from '@apollo/react-hooks';
import { connect } from 'react-redux';
import { gql } from 'apollo-boost';
import { useHistory } from 'react-router-dom';
import * as types from '../../tools/redux/actions';
import HeaderFooterSilder from '../../components/LayoutDefault/HeaderFooterSilder/HeaderFooterSilder';


const EDIT_MONEY = gql`
  mutation EditMoney($money:String!){
    editMoney(money:$money)
  }
`;
const { MonthPicker } = DatePicker;
const PayOut = (props) => {
  const { getFieldDecorator } = props.form;
  const { user, saveUser } = props;
  const [money, setMoney] = useState(20000);
  const [editMoney] = useMutation(EDIT_MONEY);
  const history = useHistory();

  if (user.role === 'STUDENT') {
    history.push('/');
  }

  function onChange(value) {
    console.log('changed', value);
    setMoney(value);

    if (value < 20000) {
      setMoney(20000);
    }
  }
  const handlesubmit = (events) => {
    events.preventDefault();
    if (!user.money) {
      notification.error({
        message: 'Số tiền trong tài khoản không đủ',
      });
      return;
    }
    const a = parseInt(user.money, 10) - money;
    props.form.validateFields((err) => {
      if (!err) {
        if (a < 0) {
          notification.error({
            message: 'Số tiền trong tài khoản không đủ',
          });
        } else {
          editMoney({
            variables: {
              money: a.toString(),
            },
          });
          saveUser({
            ...user,
            money: a,
          });
          notification.success({
            message: 'Rút tiền thành công',
          });
        }
      }
    });
  };
  return <HeaderFooterSilder>
    <div >
      <div style={{ background: '#ECECEC' }}>
        <Card
          title="Rút tiền từ tài khoản"
          width={720}
          bodyStyle={{ paddingBottom: 80 }}
        >
          <Form layout="vertical" hideRequiredMark>
            <Row gutter={16}>
              <Col >
                <Form.Item label="Số thẻ *">
                  {getFieldDecorator('sothe', {
                    rules: [{ required: true, message: 'Vui lòng nhập số thẻ' }],
                  })(<InputNumber style={{ width: '100%' }} min={100000000} max={999999999999} placeholder="123 123 123 123" />)}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Hết hạn *">
                  {getFieldDecorator('time', {
                    rules: [{ required: true, message: 'Vui lòng nhập ngày hết hạn' }],
                  })(<MonthPicker style={{ width: '100%' }} placeholder="Select month" />)}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="CVV *">
                  {getFieldDecorator('url', {
                    rules: [{ required: true, message: 'Nhập số thẻ' }],
                  })(
                    <InputNumber style={{ width: '100%' }} min={100} max={999} placeholder="123 " />,
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col >
                <Form.Item label="Họ tên *">
                  {getFieldDecorator('name', {
                    rules: [{ required: true, message: 'Vui lòng nhập họ tên' }],
                  })(<Input maxLength='40' placeholder="Nguyễn Văn A" />)}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={12}>
              <Col >
                <InputNumber
                  style={{ width: '100%' }}

                  value={money}
                  formatter={(value) => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                  parser={(value) => value.replace(/\$\s?|(,*)/g, '')}
                  onChange={onChange}
                />
              </Col>
            </Row>
          </Form>
          <div
            style={{
              position: 'absolute',
              right: 0,
              bottom: 0,
              width: '100%',

              padding: '10px 16px',
              background: '#fff',
              textAlign: 'right',
            }}
          >

            <Button onClick={handlesubmit} type="primary">
              Hoàn tất
            </Button>
          </div>


        </Card>
      </div>

    </div>
  </HeaderFooterSilder>;
};
const mapStateToProps = (state) => ({
  user: state.user,
});
// eslint-disable-next-line arrow-body-style
const maptoProps = (dispatch) => {
  return {
    saveUser: (value) => dispatch(types.saveUser(value)),
  };
};
const App = Form.create()(PayOut);
export default connect(mapStateToProps, maptoProps)(App);
