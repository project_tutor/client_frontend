/* eslint-disable indent */
/* eslint-disable no-trailing-spaces */
/* eslint-disable lines-between-class-members */
/* eslint-disable no-unused-vars */
import React, { useState, useReducer, useEffect } from 'react';
import {
  Upload, Icon, message, Input, Typography,
  Form, Button, Checkbox, Col, Select, Row, DatePicker,
} from 'antd';
import './index.css';
import { userInfo } from 'os';

const { Option } = Select;
const { Title, Paragraph, Text } = Typography;
function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}
function beforeUpload(file) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
}
const Body = (props) => {
  const [loading, setLoading] = useState('https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png');
  const [imageUrl, setimageUrl] = useState('');

  const {
    location, setLocation,
    setFullname, fullname,
    setNumber, number,
    phone, setPhone,
    level, setLevel,
  } = props;
  const uploadButton = (
    <div>
      <Icon type={loading ? 'loading' : 'plus'} />
      <div className="ant-upload-text">Upload</div>
    </div>
  );
  const handleClickNext = () => {
    props.form.validateFields((err, values) => {
      if (!err) {
        location.city = values.city;
        location.address = values.address;
        setLocation(location);
        setLevel(values.level);
        setFullname(values.fullname);
        setPhone(values.phone);
        setNumber(number + 1);
      }
    });
  };
  const { getFieldDecorator } = props.form;
  const handleChange = (event) => {
    event.preventDefault();

    // props.form.validateFields((err, values) => {
    //   if (!err) {
    //     location.city = values.city;
    //     location.address = values.address;
    //     setLocation(location);
    //     setLevel(values.level);
    //     setFullname(values.fullname);
    //     setPhone(values.phone);
    //     console.log('=========xxxx===========================');
    //     console.log(values.fullname);
    //     console.log(values.phone);
    //     console.log('============xxx========================');
    //   }
    // });
  };
  return (
    <div>
      <div className="box">
        <div>
          <Upload
            name="avatar"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
            beforeUpload={beforeUpload}
          >
            {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
          </Upload>

        </div>
      </div>
      <Form className="login-form">
        <Form.Item id="fullname" label="Họ tên" >
          {getFieldDecorator('fullname', {
            initialValue: fullname,
            rules: [{ required: true, message: 'Vui lòng nhập họ tên' }],
          })(
            <Input required={true}
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Họ tên" onChange={handleChange}
            />,
          )}
        </Form.Item>
        <Row gutter={16}>
          <Col >
            <Form.Item id="phone" label="Số điện thoại" >
              {getFieldDecorator('phone', {
                initialValue: phone,
                rules: [{ required: true, message: 'Vui lòng nhập số điện thoại' }],
              })(
                <Input required={true}
                  prefix={<Icon type="phone" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  type="text"
                  placeholder="Số điện thoại"
                  onChange={handleChange}
                />,
              )}
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col>
            <Form.Item id="level" label="Trình độ">
              {getFieldDecorator('level',
                {
                  initialValue: level || [],
                  rules: [{ required: true, message: 'Vui lòng chọn trình độ' }],
                })(
                  <Select
                    placeholder="Vui lòng chọn trình độ">
                    <Option value="Cao đẳng">Cao đẳng</Option>
                    <Option value="Đại học">Đại học</Option>
                    <Option value="Cao học">Cao học</Option>
                    <Option value="Thạc sĩ">Thạc sĩ</Option>
                    <Option value="Tiến sĩ">Tiến sĩ</Option>
                  </Select>,
                )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col>
            <Form.Item id="city" label="Thành phố">
              {getFieldDecorator('city', {
                initialValue: location.city || [],
                rules: [{ required: true, message: 'Vui lòng nhập thành phố' }],
              })(
                <Select

                  placeholder="Vui lòng nhập thành phố'">
                  <Option value="Hồ Chí Minh">Hồ Chí Minh</Option>
                  <Option value="Hà Nội">Hà Nội</Option>
                  <Option value="Cần Thờ">Cần Thơ</Option>
                  <Option value="Đà Nẵng">Đà Nẵng</Option>
                </Select>,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col >
            <Form.Item id="address" label="Địa chỉ" >
              {getFieldDecorator('address', {
                initialValue: location.address,
                rules: [{ required: true, message: 'Vui lòng nhập địa chỉ' }],
              })(
                <Input required={true}
                  prefix={<Icon type="home" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  type="text"
                  placeholder="Địa chỉ"
                  onChange={handleChange}
                />,
              )}
            </Form.Item>
          </Col>
        </Row>

      </Form>

      <div className="parent">
        <div className="child">
          <div> <Button type="danger" onClick={handleClickNext} >Tiếp theo</Button></div>
        </div>
      </div>
    </div>
  );
};
const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(Body);
export default WrappedNormalLoginForm;
