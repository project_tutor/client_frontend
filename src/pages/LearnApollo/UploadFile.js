import React from 'react';
import { gql } from 'apollo-boost';
import { useMutation } from '@apollo/react-hooks';

const UPLOAD_FILE = gql`
  mutation UploadFile($file: Upload!) {
    uploadFile(file: $file) {
      filename
      path
    }
  }
`;

function UploadFile() {
  const [uploadFile] = useMutation(UPLOAD_FILE);

  const handleUploadFile = (file) => {
    console.log(file);
    uploadFile({
      variables: {
        file,
      },
    }).then((req) => {
      console.log(req);
    }).catch((err) => {
      console.log(err);
    });
  };

  return (
    <form encType="multipart/form-data">
      <input
        type="file"
        required
        onChange={(value) => handleUploadFile(value.target.files[0])}
      />
    </form>
  );
}

export default UploadFile;
