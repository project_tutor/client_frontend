import {
  notification,
  Drawer, Form,
  Button, Col,
  Row, Input,
  DatePicker,
  Icon,
  Skeleton,
  List,
} from 'antd';
import React, { useState } from 'react';

const moment = require('moment');

const DrawerForm = (props) => {
  const [visible, setVisible] = useState(false);
  const [index, setIndex] = useState(-1);
  const [edit, setEdit] = useState(false);
  const [DateX, setDate] = useState([]);
  const {
    history,
    setHistory,
    number, setNumber, ComformData,
  } = props;

  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };

  const handleSubmit = (events) => {
    events.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        onClose();
        console.log('Received values of form: ', values);
        if (edit === true) {
          history[index].title = values.title;
          history[index].workPlace = values.workPlace;
          history[index].description = values.description;

          const ss = moment(values.dateTime[0]).format('DD-MM-YYYY');
          const ss2 = moment(ss, 'DD-MM-YYYY');
          const ss1 = moment(values.dateTime[1]).format('DD-MM-YYYY');
          const ss21 = moment(ss1, 'DD-MM-YYYY');
          const arr = [ss2, ss21];
          setDate(arr);
          // eslint-disable-next-line prefer-destructuring
          history[index].timeStart = ss;
          // eslint-disable-next-line prefer-destructuring
          history[index].timeEnd = ss1;
          setEdit(false);
          setIndex(-1);
          notification.success({
            message: 'Edit success',
          });
        } else {
          const ss = moment(values.dateTime[0]).format('DD-MM-YYYY');
          const ss2 = moment(ss, 'DD-MM-YYYY');
          const ss1 = moment(values.dateTime[1]).format('DD-MM-YYYY');
          const ss21 = moment(ss1, 'DD-MM-YYYY');
          const arr = [ss2, ss21];
          setDate(arr);
          history.push({
            title: values.title,
            workPlace: values.workPlace,
            description: values.description,
            timeStart: ss,
            timeEnd: ss1,
          });
          notification.success({
            message: 'Add success',
          });
          console.log('====================================');
          console.log(history);
          console.log('====================================');
        }
        setHistory(history);

        props.form.resetFields();
      }
    });
  };


  const { getFieldDecorator } = props.form;
  const handleEdit = (id) => {
    setEdit(true);
    setIndex(id);
    showDrawer();
  };
  const handleBack = () => {
    setHistory(history);
    setNumber(number - 1);
  };
  const handleNext = () => {
    setHistory(history);
    ComformData();
    setNumber(number + 1);
  };
  const handleRemove = (id) => {
    const list = history.filter((item, idx) => idx !== id);
    setHistory(list);
    notification.success({
      message: 'Remove success',
    });
  };
  return (
    <div>
      {/* <p>Add the schools you attended, areas of study, and degrees earned.</p> */}
      {history.map((value, idx) => (
        <List.Item key={idx}
          actions={[<Button type="primary" onClick={() => handleEdit(idx)} shape="circle" icon="edit" />, <Button onClick={() => handleRemove(idx)} type="primary" shape="circle" icon="delete" />]}
        >
          <Skeleton avatar title={false} loading={false} active>
            <List.Item.Meta
              title={<span style={{ fontWeight: 'bold' }} >{value.workPlace}</span>}
              description={`Chức vụ: ${value.title}`}
            />
            <div>{value.timeStart} - {value.timeEnd} </div>
          </Skeleton>
        </List.Item>
      ))}

      <Button type="primary" onClick={showDrawer}>
        <Icon type="plus" /> Thêm hồ sơ
        </Button>
      <Drawer
        title="Thông tin chi tiết"
        width={720}
        onClose={onClose}
        visible={visible}
        bodyStyle={{ paddingBottom: 80 }}
      >
        <Form layout="vertical" hideRequiredMark>
          <Row gutter={16}>
            <Col >
              <Form.Item label="Trường">
                {getFieldDecorator('workPlace', {

                  initialValue: edit ? history[index].workPlace : null,
                  rules: [{ required: true, message: 'Vui lòng nhập tên Trường' }],
                })(<Input maxLength='40' placeholder="Vui lòng nhập tên Trường" />)}
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col >
              <Form.Item label="Chức vụ">
                {getFieldDecorator('title', {
                  initialValue: edit ? history[index].title : null,
                  rules: [{ required: true, message: 'Vui lòng nhập chức vụ' }],
                })(
                  <Input maxLength='40' placeholder="Vui lòng nhập chức vụ" />,
                )}
              </Form.Item>
            </Col>

          </Row>
          <Row gutter={16}>
            <Col >
              <Form.Item label="Thời gian làm việc">
                {getFieldDecorator('dateTime', {
                  initialValue: edit ? DateX : null,
                  rules: [{ required: true, message: 'Vui lòng chọn thời gian làm việc' }],
                })(
                  <DatePicker.RangePicker
                    format="YYYY-MM"

                    style={{ width: '100%' }}
                    getPopupContainer={(trigger) => trigger.parentNode}
                  />,
                )}
              </Form.Item>
            </Col>

          </Row>
          <Row gutter={16}>
            <Col span={24}>
              <Form.Item label="Mô tả chi tiết">
                {getFieldDecorator('description', {
                  initialValue: edit ? history[index].description : null,
                  rules: [
                    {
                      required: true,
                      message: 'Vui lòng nhập mô tả',
                    },
                  ],
                })(<Input.TextArea rows={4} placeholder="please enter url description" />)}
              </Form.Item>
            </Col>
          </Row>
        </Form>
        <div
          style={{
            position: 'absolute',
            right: 0,
            bottom: 0,
            width: '100%',
            borderTop: '1px solid #e9e9e9',
            padding: '10px 16px',
            background: '#fff',
            textAlign: 'right',
          }}
        >
          <Button onClick={onClose} style={{ marginRight: 8 }}>
            Hủy
            </Button>
          <Button onClick={handleSubmit} type="primary">
            Đồng ý
            </Button>
        </div>
      </Drawer>
      <div className="parent">
        <div className="child">
          <div> <Button type="danger" onClick={handleNext} >Hoàn tất</Button></div>
        </div>
        <Button type="link" onClick={handleBack}>Trở về</Button>
      </div>
    </div>
  );
};

const App = Form.create()(DrawerForm);
export default App;
