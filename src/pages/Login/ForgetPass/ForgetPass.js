import React from 'react';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import {
  Col,
  Button,
  Avatar,
  Form,
  Input,
  notification,
} from 'antd';
import { gql } from 'apollo-boost';
import LayoutHeaderFooter from '../../../components/LayoutDefault/HeaderFooter/HeaderFooter';
import '../style.css';

const ForgetPassword = gql`
    mutation ForgotPassword($input: String!) {
        forgotPassword(email: $input)
    }
`;

const ResetPassWord = gql`
    mutation ResetPassword($token: String!, $newPassword: String!) {
    resetPassword(token: $token, newPassword: $newPassword)
  }
`;

class ForgetPass extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      resetPass: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.openNotificationWithIcon = this.openNotificationWithIcon.bind(this);
  }

  componentDidMount() {
    const { match } = this.props;
    const { token } = match.params;
    if (token) {
      this.setState({ resetPass: true });
    }
  }

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('password')) {
      callback('Mật khẩu không trùng khớp!');
    } else {
      callback();
    }
  }

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value.length < 6) {
      callback('Mật khẩu phải có ít nhất 6 ký tự');
    }
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFields((err1, values) => {
      if (!err1) {
        this.setState({ loading: true }, () => {
          const { history } = this.props;
          const { email } = values;
          this.props.client.mutate({
            mutation: ForgetPassword,
            variables: {
              input: email,
            },
          }).then((req) => {
            if (req) {
              this.setState({ loading: false });
              this.openNotificationWithIcon('success', 'Email đặt lại mật khẩu đã được gửi tới tài khoản của bạn!');
              history.replace('/login');
            }
          }).catch((err) => {
            this.setState({ loading: false });
            this.openNotificationWithIcon('error', err.message.substring(15));
          });
        });
      }
    });
  }

  handleReset = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err1, values) => {
      if (!err1) {
        this.setState({ loading: true }, () => {
          const { history, match } = this.props;
          const { password } = values;
          const { token } = match.params;
          this.props.client.mutate({
            mutation: ResetPassWord,
            variables: {
              token,
              newPassword: password,
            },
          }).then((req) => {
            if (req) {
              this.setState({ loading: false });
              this.openNotificationWithIcon('success', 'Mật khẩu của bạn đã được cập nhật!');
              history.replace('/login');
            }
          }).catch((err) => {
            this.setState({ loading: false });
            this.openNotificationWithIcon('error', err.message.substring(15));
          });
        });
      }
    });
  }

  openNotificationWithIcon(type, description) {
    notification[type]({
      message: 'Thông báo',
      description,
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { resetPass, loading } = this.state;

    const reset = (resetPass && <div style={{ textAlign: 'left', display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                <h1>Cập nhật mật khẩu</h1>
                <h3 style={{ fontSize: 15, width: '70%' }}>Nhập vào mật khẩu mới của bạn.</h3>
                <Form style={{ width: '70%' }} onSubmit={this.handleReset}>
                    <Form.Item className="mInput" hasFeedback>
                        {getFieldDecorator('password', {
                          rules: [
                            {
                              required: true,
                              message: 'Mật khẩu không được bỏ trống!',
                            },
                            {
                              validator: this.validateToNextPassword,
                            },
                          ],
                        })(<Input.Password disabled={loading} type="password" placeholder="Nhập vào mật khẩu" onBlur={this.handleConfirmBlur} />)}
                    </Form.Item>
                    <Form.Item className="mInput" hasFeedback >
                        {getFieldDecorator('confirm', {
                          rules: [
                            {
                              required: true,
                              message: 'Xin vui lòng xác nhận mật khẩu!',
                            },
                            {
                              validator: this.compareToFirstPassword,
                            },
                          ],
                        })(<Input disabled={loading} onBlur={this.handleConfirmBlur} type="password" placeholder="Xác nhận mật khẩu" />)}
                    </Form.Item>
                    <Button style={{ width: '100%', background: 'orange' }} htmlType="submit" loading={this.state.loading}>Reset Password</Button>
                </Form>
            </div>
    );

    const sendEmail = (!resetPass && <div style={{ textAlign: 'center', display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
            <h1>Bạn quên mật khẩu?</h1>
            <h3 style={{ fontSize: 15, width: '70%' }}>Điều đó không có gì đáng lo cả, nó xảy ra hàng ngày thôi! Hãy nhập vào E-mail bạn dùng để đăng ký tài khoản để cập nhật lại mật khẩu của bạn.</h3>
            <Form style={{ width: '70%' }} onSubmit={this.handleSubmit}>
                <Form.Item style={{ width: '100%' }}>
                    {getFieldDecorator('email', {
                      rules: [
                        {
                          type: 'email',
                          message: 'The input is not valid E-mail!',
                        },
                        {
                          required: true,
                          message: 'Please input your E-mail!',
                        },
                      ],
                    })(<Input placeholder="Nhập vào E-mail của bạn" />)}
                </Form.Item>
                <Button style={{ width: '100%', background: 'orange' }} htmlType="submit" loading={this.state.loading}>Gửi Email</Button>
            </Form>
        </div>

    );

    return (
    <LayoutHeaderFooter>
        <div className="Login">
            <Col offset={7} span={18}>
                <div className='forgetpass'>
                    <Avatar src="https://i.imgur.com/43iwhPV.png" shape="square" size={150}></Avatar>

                    {!resetPass ? sendEmail : reset}
                </div>
            </Col>
        </div>
    </LayoutHeaderFooter>
    );
  }
}


export default Form.create({})(withApollo(withRouter(ForgetPass)));
