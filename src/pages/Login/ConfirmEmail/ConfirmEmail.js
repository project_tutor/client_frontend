import React, { useEffect, useState } from 'react';
import { useRouteMatch, useHistory } from 'react-router-dom';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import {
  Col,
  Button,
  Avatar,
  Icon,
} from 'antd';
import LayoutHeaderFooter from '../../../components/LayoutDefault/HeaderFooter/HeaderFooter';
import '../style.css';

const Confirm_User = gql`
  mutation ConfirmEmail($token:String!){
  confirmEmail(token: $token)
}
`;

const ConfirmEmail = () => {
  const history = useHistory();
  const match = useRouteMatch();
  const { token } = match.params;
  const [isRegister, setRegisters] = useState(false);
  const [isSuccess, setSuccess] = useState(false);
  const [isFail, setFail] = useState(false);


  const [confirmEmail] = useMutation(Confirm_User);
  // notification.success({
  //   message: 'Xác minh tài khoản thành công!',
  // });
  useEffect(() => {
    if (!isRegister) {
      confirmEmail({ variables: { token } }).then(() => {
        setSuccess(true);
        setFail(false);
      }).catch(() => {
        setSuccess(false);
        setFail(true);
      });
      setRegisters(false);
    }
  }, [isRegister, confirmEmail, token]);

  const ReturnToLogin = () => {
    history.replace('/login');
  };

  const Success = (
    <div>
      <Icon style={{ color: 'green', fontSize: 30 }} type="check-circle" />
      <h1>Xác nhận E-mail thành công!</h1>
      <h3 style={{ fontSize: 15 }}>Chúc mừng bạn đã xác nhận E-mail thành công! Bạn đã có thể sử dụng tài khoản của mình để đăng nhập.</h3>
      < Button style={{ width: '100%', background: 'orange' }} onClick={ReturnToLogin}>Return to Login</Button>
    </div>
  );

  const fail = (
    <div>
      <Icon style={{ color: 'red', fontSize: 30 }} type="exclamation-circle" />
      <h1>Xác nhận E-mail không thành công!</h1>
      <h3 style={{ fontSize: 15 }}>Đã có lỗi xãy ra, chúng tôi rất tiếc cho sự cố này. Xin vui lòng thử lại!.</h3>
      < Button style={{ width: '100%', background: 'orange' }} onClick={ReturnToLogin}>Return to Login</Button>
    </div>
  );

  return (
    <LayoutHeaderFooter>
      <div className="Login">
        <Col offset={7} span={18}>
          <div className='forgetpass' style={{ textAlign: 'center' }}>
              <Avatar src="https://i.imgur.com/43iwhPV.png" shape="square" size={150}></Avatar>
              { isSuccess && !isFail && Success}
              { isFail && !isSuccess && fail}
          </div>
        </Col>
      </div>
    </LayoutHeaderFooter>
  );
};

export default ConfirmEmail;
