import React from 'react';
import { Steps } from 'antd';
import 'antd/dist/antd.css';

const { Step } = Steps;
const header = (props) => {
  const { vitri } = props;
  return (
    <Steps style={{ padding: '10px' }} size="small" current={vitri}>
      <Step title="Thông tin" description="Thông tin cá nhân " />
      <Step title="Học vấn" description="Trình độ học vấn " />
      <Step title="Kinh nghiệm" description="Kinh nghiệm làm việc" />
    </Steps>
  );
};

export default header;
