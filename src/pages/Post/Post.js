import React from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';

import HeaderFooterSilder from '../../components/LayoutDefault/HeaderFooterSilder/HeaderFooterSilder';
import SubmitForm from './components/SubmitForm';

const Post = (props) => {
  const history = useHistory();

  const { user } = props;
  if (user.role === 'STUDENT') {
    history.push('/');
  }
  return <HeaderFooterSilder>
    <SubmitForm />
  </HeaderFooterSilder>;
};

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps, null)(Post);
