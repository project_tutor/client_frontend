/* eslint-disable no-underscore-dangle */
/* eslint-disable indent */
import React, { useState } from 'react';
import { Select, Form, Icon, Input, Button, PageHeader, Upload, message, Row, Col, notification } from 'antd';
import { connect } from 'react-redux';
import { gql } from 'apollo-boost';
import { useMutation } from '@apollo/react-hooks';
import HeaderFooterSilder from '../../components/LayoutDefault/HeaderFooterSilder/HeaderFooterSilder';
import * as actions from '../../tools/redux/actions';

const { Option } = Select;

const EDIT_USER = gql`
  mutation EditUser($userID: ID!, $input: UserInput!, $imageFile: Upload){
  editUser(
    userID: $userID,
    input: $input,
    imageFile: $imageFile
  ){
    fullname
    imageURL
    location{
      address
      city
    }
    phone
  }
}
`;


function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
}

const url_backend = process.env.REACT_APP_URL_BACKEND_API ? 'https://tutor-client-backend.herokuapp.com' : 'http://localhost:3001';

const Profile = (props) => {
  const { form, user, handleSaveUser } = props;
  const { getFieldDecorator } = form;
  const [dataImage, setDataImage] = useState({ imageUrl: url_backend + user.imageURL, loading: false });
  const [editUser] = useMutation(EDIT_USER);
  const [dataFile, setDataFile] = useState(null);

  // Avatar
  const handleChange = (info) => {
    if (info.file.status === 'uploading') {
      setDataImage({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      setDataFile(info.file.originFileObj);
      getBase64(info.file.originFileObj, (imageUrl) => {
        setDataImage({
          imageUrl,
          loading: false,
        });
      });
    }
  };

  const uploadButton = (
    <div>
      <Icon type={dataImage.loading ? 'loading' : 'plus'} />
      <div className="ant-upload-text">Upload</div>
    </div>
  );

  // Submit Form
  const handleSubmit = (e) => {
    e.preventDefault();
    form.validateFields((err, values) => {
      if (!err) {
        // console.log('Received values of form: ', values);
        // console.log(dataFile);

        editUser({
          variables: {
            userID: user._id,
            input: {
              fullname: values.fullname,
              phone: values.phone,
              location: {
                address: values.address,
                city: values.city,
              },
              // imageURL: dataImage.imageUrl,
            },
            imageFile: dataFile,
          },
        }).then((req) => {
          const userReq = req.data.editUser;
          handleSaveUser({ ...user, ...userReq });
          notification.success({
            message: 'Cập nhật thành công!',
          });
        });
      }
    });
  };

  return (
    <HeaderFooterSilder>
      <PageHeader
        style={{
          border: '1px solid rgb(235, 237, 240)',
        }}
        title="Thông tin"
      />
      <Form className="login-form" onSubmit={handleSubmit} style={{ width: '100%', padding: '20px' }}>
        <Row type="flex" justify="space-around" align="middle" style={{ textAlign: 'center', padding: '5px' }}>
          <Col>
            <Upload
              name="avatar"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
              beforeUpload={beforeUpload}
              onChange={handleChange}
            >
              {dataImage.imageUrl ? <img src={dataImage.imageUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
            </Upload>
          </Col>
        </Row>
        <Form.Item id="fullname" label="Họ tên" >
          {getFieldDecorator('fullname', {
            initialValue: user.fullname,
            rules: [{ required: true, message: 'Vui lòng nhập họ tên' }],
          })(
            <Input required={true}
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Họ tên"
            />,
          )}
        </Form.Item>
        <Form.Item id="phone" label="Số điện thoại" >
          {getFieldDecorator('phone', {
            initialValue: user.phone || '',
            rules: [{ required: true, message: 'Vui lòng nhập số điện thoại' }],
          })(
            <Input
              prefix={<Icon type="phone" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="text"
              placeholder="Số điện thoại"

            />,
          )}
        </Form.Item>
        <Form.Item id="city" label="Thành phố">
          {getFieldDecorator('city', {
            initialValue: user.location ? user.location.city : '',
            rules: [{ required: true, message: 'Vui lòng nhập thành phố' }],
          })(
            <Select placeholder="Select a person">
              <Option value="Hồ Chí Minh">Hồ Chí Minh</Option>
              <Option value="Hà Nội">Hà Nội</Option>
              <Option value="Đà Nẵng">Đà Nẵng</Option>
              <Option value="Cần Thờ">Cần Thơ</Option>
            </Select>,
          )}
        </Form.Item>
        <Form.Item id="address" label="Địa chỉ" >
          {getFieldDecorator('address', {
            initialValue: user.location ? user.location.address : '',
            rules: [{ required: true, message: 'Vui lòng nhập địa chỉ' }],
          })(
            <Input
              prefix={<Icon type="home" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="text"
              placeholder="Địa chỉ"
            />,
          )}
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button">
            Cập nhật
            </Button>
        </Form.Item>
      </Form>
    </HeaderFooterSilder>
  );
};

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
  handleSaveUser: (user) => {
    dispatch(actions.saveUser(user));
  },
});

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(Profile);

export default connect(mapStateToProps, mapDispatchToProps)(WrappedNormalLoginForm);
