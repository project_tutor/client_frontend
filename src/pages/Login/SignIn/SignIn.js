import React from 'react';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';
import {
  Form, Input, Button, Icon,
} from 'antd';

class SignIn extends React.Component {
  constructor(props) {
    super(props);

    this.handleLogin = this.handleLogin.bind(this);
    this.handleLoginByFacebook = this.handleLoginByFacebook.bind(this);
    this.handleLoginByGoogle = this.handleLoginByGoogle.bind(this);
  }

  // componentWillReceiveProps() {
  //   if (this.props.goToRegister) {
  //     this.props.form.setFieldsValue({
  //       email1: '',
  //       password1: '',
  //     });
  //   }
  // }

  handleLogin(e) {
    const { handleLogin } = this.props;
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        handleLogin(values);
      }
    });
  }

  handleLoginByFacebook(response) {
    const { handleLogin } = this.props;
    if (!response.status) {
      handleLogin({ email1: response.email, password1: response.id, isSocial: true });
    }
  }

  handleLoginByGoogle(response) {
    const { handleLogin } = this.props;
    const { profileObj } = response;
    if (profileObj) {
      handleLogin({ email1: profileObj.email, password1: profileObj.googleId, isSocial: true });
    }
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { loading } = this.props;
    return (
      <div className="form-container sign-in-container">
        <Form onSubmit={this.handleLogin} className="login-form">
          <h1>Đăng nhập</h1>
          <Form.Item >
            <div className="social-container">
              <FacebookLogin
                appId="572752733493843" // APP ID NOT CREATED YET
                fields="name,email,picture"
                callback={this.handleLoginByFacebook}
                cssClass="social-button"
                icon="fa-facebook-f"
                textButton=""
                autoLoad={false}
                isDisabled={loading}
              />
              <GoogleLogin
                clientId="264218692025-aqmc5agm1fmun0d793hl39ctugaa8jgu.apps.googleusercontent.com"
                render={(renderProps) => (
                  <Button type="danger" shape="circle" ghost className="btn" onClick={renderProps.onClick} disabled={renderProps.disabled || loading}>
                    <Icon type="google" />
                  </Button>
                )}
                onSuccess={this.handleLoginByGoogle}
                onFailure={this.handleLoginByGoogle}
              />
            </div>
            <span>hoặc sử dụng tài khoản của bạn</span>
          </Form.Item>
          <Form.Item className="mInput">
            {getFieldDecorator('email1', {
              rules: [
                {
                  type: 'email',
                  message: 'Không đúng định dạng E-mail!',
                },
                {
                  required: true,
                  message: 'E-mail không được bỏ trống!',
                },
              ],
            })(<Input disabled={loading} prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email" className="mInput" />)}
          </Form.Item>
          <Form.Item className="mInput">
            {getFieldDecorator('password1', {
              rules: [{ required: true, message: 'Mật khẩu không được bỏ trống!' }],
            })(
              <Input
                disabled={loading}
                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                type="password"
                placeholder="Mật khẩu"
              />,
            )}
          </Form.Item>

          {/* <Form.Item className="mInput">
                      {getFieldDecorator('remember', {
                        valuePropName: 'checked',
                        initialValue: false,
                      })(<Checkbox disabled={loading}>Lưu tài khoản</Checkbox>)}
                    </Form.Item> */}
          <Button style={{ width: '70%' }} type="primary" htmlType="submit" className="login-form-button" loading={loading}>
            Đăng nhập
          </Button>
          <a disabled={loading} href="/forgetpassword">Quên mật khẩu?</a>
        </Form>
      </div>
    );
  }
}
const Login = Form.create({ name: 'Login' })(SignIn);
export default Login;
