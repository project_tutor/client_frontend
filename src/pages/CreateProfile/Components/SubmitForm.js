/* eslint-disable no-underscore-dangle */
/* eslint-disable indent */
import React, { useState } from 'react';
import {
  Tag, Input, Tooltip, Icon, List, notification,
  Skeleton, Avatar, Card, Button,
  Drawer, Form, Row, Col,
} from 'antd';
import { useMutation } from '@apollo/react-hooks';
import { connect } from 'react-redux';
import { gql } from 'apollo-boost';

const EDIT_USER = gql`
  mutation EditUser($userID: ID!, $input: UserInput!){
  editUser(userID: $userID, input: $input){
    fullname
  }
}
`;
const { Meta } = Card;

const Index = (props) => {
  const { user } = props;
  const [tags, setTag] = useState(['Unremovable']);
  const [inputVisible, setInputVisible] = useState(false);
  const [inputValue, setInputValue] = useState('');
  const [visible, setVisible] = useState(false);
  const [title, setTitle] = useState('Tiêu đề của bạn');
  const [des, setDes] = useState('Mô tả chi tiết ');
  const [hourlyRate, setHourlyRate] = useState('Mô tả chi tiết ');
  const [editUser] = useMutation(EDIT_USER);

  const handleClose = (removedTag) => {
    const newTag = tags.filter((tag) => tag !== removedTag);
    console.log(newTag);
    setTag(newTag);
  };
  const showInput = () => {
    setInputVisible(true);
  };
  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };
  const handleSubmit = (events) => {
    events.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        onClose();
        setTitle(values.title);
        setDes(values.descrip);
        setHourlyRate(values.hocphi);
      }
    });
  };
  const { getFieldDecorator } = props.form;
  const handleInputChange = (e) => {
    setInputValue(e.target.value);
  };
  const handleInputConfirm = () => {
    if (inputValue && tags.indexOf(inputValue) === -1) {
      setTag([...tags, inputValue]);
    }
    console.log(tags);
    setInputVisible(false);
    setInputValue(false);
    setInputValue('');
  };
  const {
    education, history, fullname, location,
  } = props;
  const handleFinish = () => {
    const skills = [];
    tags.forEach((va, idx) => {
      skills[idx] = {
        name: va,
        isVisbale: true,
      };
    });
    editUser({
      variables: {
        userID: user._id,
        input: {
          skill: skills,
          hourlyRate,
        },
      },
    });
    notification.success({
      message: 'Update success',
    });
  };
  const loading = false;

  return (
    <div>
      <Card
        title="Thông tin cá nhân"
        extra={<Button onClick={showDrawer} type="dashed" size="small" shape="circle" icon="edit" />}
        style={{ width: 'auto', marginTop: 16 }}
      >
        <Skeleton avatar loading={loading} active
        >
          <Meta
            avatar={
              <Avatar src={user.imageURL} />
            }
            title={fullname}
            description={` ${location.address},${location.city} `}
          >
          </Meta>
        </Skeleton>

        <p style={{ marginTop: '10px', fontWeight: 'bold', wordWrap: 'break-word' }}><label style={{ marginRight: '10px' }}>{title}</label>
        </p>
        <div style={{ width: 'auto', wordWrap: 'break-word' }} >
          <p ><label style={{ marginRight: '10px' }}></label>{des}</p>
        </div>
      </Card>
      <Card title="Kỹ năng" style={{ width: 'auto', marginTop: 5 }}>
        <div>
          {tags.map((tag, index) => {
            const isLongTag = tag.length > 20;
            const tagElem = (
              <Tag color="#87d068" key={tag} closable={index !== 0} onClose={() => handleClose(tag)}>
                {isLongTag ? `${tag.slice(0, 20)}...` : tag}
              </Tag>
            );
            return isLongTag ? (
              <Tooltip title={tag} key={tag}>
                {tagElem}
              </Tooltip>
            ) : (
                // eslint-disable-next-line indent
                tagElem
                // eslint-disable-next-line indent
              );
          })}
          {inputVisible && (
            <Input
              autoFocus
              type="text"
              size="small"
              style={{ width: 78 }}
              value={inputValue}
              onChange={handleInputChange}
              onBlur={handleInputConfirm}
              onPressEnter={handleInputConfirm}
            />
          )}
          {!inputVisible && (
            <Tag onClick={showInput} style={{ background: '#fff', borderStyle: 'dashed' }}>
              <Icon type="plus" /> New Tag
          </Tag>
          )}
        </div>
      </Card>
      <Card title="Kinh nghiệm" style={{ width: 'auto', marginTop: 5 }}>
        <div>
          {/* <p>Add the schools you attended, areas of study, and degrees earned.</p> */}
          {history.map((value, idx) => (
            <List.Item key={idx}
            >
              <Skeleton avatar title={false} loading={false} active>
                <List.Item.Meta
                  title={<span style={{ fontWeight: 'bold' }} >{value.workPlace}</span>}
                  description={`Chức vụ: ${value.title}`}
                />
                <div>{value.timeStart} - {value.timeEnd} </div>
              </Skeleton>
            </List.Item>
          ))}
        </div>
      </Card>
      <Card title="Học thức" style={{ width: 'auto', marginTop: 5 }}>
        <div>
          {education.map((value, idx) => (
            <List.Item key={idx}
            >
              <Skeleton avatar title={false} loading={false} active>
                <List.Item.Meta
                  title={<span style={{ fontWeight: 'bold' }} >{value.name}</span>}
                  description={value.degree}
                />
                <div>{value.timeStart} - {value.timeEnd} </div>
              </Skeleton>
            </List.Item>
          ))}
        </div>
      </Card>
      <Drawer
        title="Thông tin chi tiết"
        width={720}
        onClose={onClose}
        visible={visible}
        bodyStyle={{ paddingBottom: 80 }}
      >
        <Form layout="vertical" hideRequiredMark>
          <Row gutter={16}>
            <Col >
              <Form.Item label="Tiêu đề">
                {getFieldDecorator('title', {
                  rules: [{ required: true, message: 'Vui lòng nhập chức vụ' }],
                })(
                  <Input maxLength='40' placeholder="Vui lòng nhập chức vụ" />,
                )}
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col >
              <Form.Item label="Mô Tả chi tiết">
                {getFieldDecorator('descrip', {
                  rules: [{ required: true, message: 'Vui lòng giới thiệu bản thân' }],
                })(
                  <Input.TextArea rows={4} placeholder="please enter url description" />,
                )}
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col >
              <Form.Item label="Học phí/H">
                {getFieldDecorator('hocphi', {
                  rules: [{ required: true, message: 'Vui lòng giới thiệu bản thân' }],
                })(
                  <Input type="text" maxLength='40' placeholder="Học phí trên giờ" />,
                )}
              </Form.Item>
            </Col>
          </Row>
        </Form>
        <div
          style={{
            position: 'absolute',
            right: 0,
            bottom: 0,
            width: '100%',
            borderTop: '1px solid #e9e9e9',
            padding: '10px 16px',
            background: '#fff',
            textAlign: 'right',
          }}
        >
          <Button onClick={onClose} style={{ marginRight: 8 }}>
            Hủy
            </Button>
          <Button onClick={handleSubmit} type="primary">
            Đồng ý
            </Button>
        </div>
      </Drawer>
      <div className="parent" style={{ width: 'auto', marginTop: 5 }}>
        <div className="child">
          <div> <Button type="danger" onClick={handleFinish} >Hoàn tất</Button></div>
        </div>

      </div>
    </div >
  );
};

const mapStateToProps = (state) => ({
  user: state.user,
});
const App = Form.create()(Index);

export default connect(mapStateToProps, null)(App);
