import React from 'react';

import Home from '../pages/Home/Home';
import Login from '../pages/Login/Login';
import ConfirmEmail from '../pages/Login/ConfirmEmail/ConfirmEmail';
import NotFound from '../pages/NotFound/NotFound';
import CreteProfile from '../pages/CreateProfile/index';
import Teacher from '../pages/Teacher/Teacher';
import Profile from '../pages/Profile/Profile';
import ChangePassword from '../pages/ChangePassword/ChangePassword';
import Post from '../pages/Post/Post';
import UploadFile from '../pages/LearnApollo/UploadFile';
import Contract from '../pages/Contract/Contract';
import PayIn from '../pages/PayIn';
import PayOut from '../pages/PayOut';
import AboutUs from '../pages/AboutUs/AboutUs';
import ForgetPass from '../pages/Login/ForgetPass/ForgetPass';
import TeacherChart from '../pages/TeacherChart/TeacherChart';
import Complaint from '../pages/Complaint/index';
import Chat from '../pages/Chat/Chat';

/**
|--------------------------------------------------
| auth: -1 unlogin, 0 : unlogin && logined, 1: logined
|--------------------------------------------------
*/

export default [
  {
    path: '/',
    exact: true,
    auth: 0,
    redirect: '/',
    component: () => <Home />,
  },
  {
    path: '/teacher/:type?/:id?',
    exact: true,
    auth: 0,
    redirect: '/',
    component: () => <Teacher />,
  },
  {
    path: '/login',
    exact: false,
    auth: -1,
    redirect: '/',
    component: () => <Login />,
  },
  {
    path: '/create-profile',
    exact: false,
    auth: 0,
    redirect: '/login',
    component: () => <CreteProfile />,
  },
  {
    path: '/confirm-email/:token',
    exact: false,
    auth: 0,
    redirect: '/',
    component: () => <ConfirmEmail />,
  },
  {
    path: '/profile',
    exact: true,
    auth: 1,
    redirect: '/',
    component: () => <Profile />,
  },
  {
    path: '/profile/change-password',
    exact: false,
    auth: 1,
    redirect: '/',
    component: () => <ChangePassword />,
  },
  {
    path: '/post',
    exact: false,
    auth: 1,
    redirect: '/',
    component: () => <Post />,
  },
  {
    path: '/contract',
    exact: false,
    auth: 1,
    redirect: '/',
    component: () => <Contract />,
  },
  {
    path: '/payout',
    exact: false,
    auth: 1,
    redirect: '/',
    component: () => <PayOut />,
  },
  {
    path: '/payin',
    exact: false,
    auth: 1,
    redirect: '/',
    component: () => <PayIn />,
  },
  {
    path: '/complaint',
    exact: false,
    auth: 1,
    redirect: '/',
    component: () => <Complaint />,
  },
  {
    path: '/UploadFile',
    exact: false,
    auth: 0,
    redirect: '/',
    component: () => <UploadFile />,
  },
  {
    path: '/about',
    exact: false,
    auth: 0,
    redirect: '/',
    component: () => <AboutUs />,
  },
  {
    path: '/forgetpassword/:token?',
    exact: false,
    auth: 0,
    redirect: '/login',
    component: () => <ForgetPass />,
  },
  {
    path: '/income',
    exact: false,
    auth: 1,
    redirect: '/',
    component: () => <TeacherChart />,
  },
  {
    path: '/chat',
    exact: false,
    auth: 1,
    redirect: '/',
    component: () => <Chat />,
  },
  {
    path: '/chat/:id',
    exact: false,
    auth: 1,
    redirect: '/',
    component: () => <Chat />,
  },
  {
    path: '*',
    exact: false,
    auth: 0,
    redirect: null,
    component: () => <NotFound />,
  },
];
