import React from 'react';
import {
  Layout,
  Menu,
  Icon,
} from 'antd';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { connect } from 'react-redux';

import LayoutDefault from '../LayoutDefault';

const HeaderFooterSilder = (props) => {
  const { children, user } = props;
  const { Content, Sider } = Layout;
  const { SubMenu } = Menu;
  const history = useHistory();
  const match = useRouteMatch();
  const { path } = match;
  const arrPath = path.split('/');
  // console.log(user);

  return (
    <LayoutDefault>
      <Layout style={{ padding: '0', background: '#fff' }}>
        <Sider width={200} style={{ background: '#fff' }}>
          <Menu
            mode="inline"
            defaultSelectedKeys={[path]}
            defaultOpenKeys={[arrPath[1]]}
            style={{ height: '100%' }}
            onClick={(e) => history.push(e.key)}
          >
            <SubMenu
              key="profile"
              title={
                <span>
                  <Icon type="user" />
                  Cá nhân
                </span>
              }
            >
              <Menu.Item key="/profile">Thông tin</Menu.Item>
              <Menu.Item key="/profile/change-password">Đổi mật khẩu</Menu.Item>
            </SubMenu>
            {user.role === 'TEACHER'
              ? <Menu.Item key="/post">
                <span>
                  <Icon type="form" />
                  Bài giới thiệu
                  </span>
              </Menu.Item> : ''
            }

            <Menu.Item key="/contract">
              <span>
                <Icon type="book" />
                Hợp đồng
                </span>
            </Menu.Item>
            <Menu.Item key="/complaint">
              <span>
                <Icon type="solution" />
                Khiếu nại
                </span>
            </Menu.Item>

            {user.role === 'STUDENT'
              ? <Menu.Item key="/payin">
                <span>
                  <Icon type="dollar" />
                  Nạp tiền
                </span>
              </Menu.Item>
              : <Menu.Item key="/payout">
                <span>
                  <Icon type="dollar" />
                  Rút tiền
                </span>
              </Menu.Item>
            }
            {
              user.role === 'TEACHER' && <Menu.Item key="/income">
                <span>
                  <Icon type="area-chart" />
                  Doanh thu
                </span>
              </Menu.Item>
            }

            {/* <SubMenu
              key="sub2"
              title={
                <span>
                  <Icon type="laptop" />
                  Bài giới thiệu
                </span>
              }
            >
              <Menu.Item key="5">option5</Menu.Item>
              <Menu.Item key="6">option6</Menu.Item>
              <Menu.Item key="7">option7</Menu.Item>
              <Menu.Item key="8">option8</Menu.Item>
            </SubMenu>
            <SubMenu
              key="sub3"
              title={
                <span>
                  <Icon type="notification" />
                  subnav 3
                </span>
              }
            >
              <Menu.Item key="9">option9</Menu.Item>
              <Menu.Item key="10">option10</Menu.Item>
              <Menu.Item key="11">option11</Menu.Item>
              <Menu.Item key="12">option12</Menu.Item>
            </SubMenu> */}
          </Menu>
        </Sider>
        <Content>
          <div style={{ background: '#fff', padding: '24px' }} className='fill-content'>{children}</div>
        </Content>
      </Layout>
    </LayoutDefault >
  );
};

const mapStateToProps = (state) => ({
  user: state.user,
});


export default connect(mapStateToProps, null)(HeaderFooterSilder);
