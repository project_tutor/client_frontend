import React from 'react';
import { withApollo } from 'react-apollo';
import { gql } from 'apollo-boost';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Col,
  Button,
  notification,
  Avatar,
} from 'antd';
import SignIn from './SignIn/SignIn';
import Register from './SignUp/SignUp';
import * as actions from '../../tools/redux/actions';
import LayoutHeaderFooter from '../../components/LayoutDefault/HeaderFooter/HeaderFooter';
import './style.css';


const LOGIN = gql`
  mutation Login($input: LoginInput!) {
    login(input: $input) {
      token
      user {
        _id
        email
        fullname
        phone
        imageURL
        location{
          city
          address
        }
        role
        money
      }
    }
  }
`;

const REGISTER = gql`
  mutation Register($input: RegisterInput!) {
    register(input: $input)
  }
`;

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      goToRegister: false,
      loading: false,
    };

    this.handleLogin = this.handleLogin.bind(this);
    this.handleGoToRegister = this.handleGoToRegister.bind(this);
    this.handleRegister = this.handleRegister.bind(this);
    this.openNotificationWithIcon = this.openNotificationWithIcon.bind(this);
  }

  handleLogin(values) {
    if (!values) return;
    const { email1, password1 } = values;
    const { history, handleSaveUser } = this.props;
    this.setState({ loading: true });
    this.props.client.mutate({
      mutation: LOGIN,
      variables: {
        input: { email: email1, password: password1 },
      },
    })
      .then((req) => {
        this.openNotificationWithIcon('success', 'Đăng nhập thành công');
        this.setState({ loading: false });
        handleSaveUser(req.data.login.user);
        localStorage.setItem('token', req.data.login.token);
        window.scrollTo(0, 0);
        if (req.data.login.user.role === 'STUDENT') {
          history.push('/teacher');
        } else {
          history.push('/post');
        }
      })
      .catch((err) => {
        this.openNotificationWithIcon('error', err.message.substring(15));
        this.setState({ loading: false });
      });
  }

  handleRegister(values, role) {
    if (!values || !role) return;
    const { email, fullname, password, isSocial, imageURL } = values;
    this.setState({ loading: true });
    this.props.client.mutate({
      mutation: REGISTER,
      variables: {
        input: {
          email, fullname, password, role, isSocial, imageURL,
        },
      },
    }).then((req) => {
      if (req) {
        this.setState({ loading: false });
        this.openNotificationWithIcon('success', 'Đăng ký thành công');
        this.setState({
          goToRegister: !this.state.goToRegister,
        });
      }
    }).catch((err) => {
      this.setState({ loading: false });
      this.openNotificationWithIcon('error', err.message.substring(15));
    });
  }

  handleGoToRegister() {
    const { loading } = this.state;
    if (loading) return;
    this.setState({
      goToRegister: !this.state.goToRegister,
    });
  }

  openNotificationWithIcon(type, description) {
    notification[type]({
      message: 'Thông báo',
      description,
    });
  }

  render() {
    const { goToRegister, loading } = this.state;
    return (
      <LayoutHeaderFooter>
        <div className="Login">
          <Col offset={6} span={18}>
            <div className={goToRegister ? 'container right-panel-active' : 'container'}>
              <Register
                handleRegister={this.handleRegister}
                loading={loading}
                goToRegister={goToRegister}
              />
              <SignIn
                handleLogin={this.handleLogin}
                loading={loading}
                goToRegister={goToRegister}
              />
              <div className="overlay-container">
                <div className="overlay">
                  <div className="overlay-panel overlay-left">
                    <Avatar src="https://i.imgur.com/43iwhPV.png" shape="square" size={120}></Avatar>
                    <h2>Chào mừng bạn đến với UberTutor!</h2>
                    <p>Hãy đăng ký để sử dụng tất cả tiện ích của chúng tôi</p>
                    <h5>Đã có tài khoản?</h5>

                    <Button className="text-white" type="primary" ghost onClick={this.handleGoToRegister}>
                      Đăng nhập
                    </Button>
                  </div>
                  <div className="overlay-panel overlay-right">
                    <Avatar src="https://i.imgur.com/43iwhPV.png" shape="square" size={120}></Avatar>
                    <h2>Chào mừng bạn đã trở lại!</h2>
                    <p>Đăng nhập để sử dụng các dịch vụ một cách tốt nhất</p>
                    <h5>Chưa có tài khoản?</h5>

                    <Button className="text-white" type="primary" ghost onClick={this.handleGoToRegister}>
                      Đăng ký
                            </Button>
                  </div>
                </div>
              </div>
            </div>
          </Col>
        </div>

      </LayoutHeaderFooter>
    );
  }
}


const mapDispatchToProps = (dispatch) => ({
  handleSaveUser: (user) => dispatch(actions.saveUser(user)),
});

export default withApollo(withRouter(connect(null, mapDispatchToProps)(Login)));
