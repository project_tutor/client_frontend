/* eslint-disable no-nested-ternary */
/* eslint-disable no-return-assign */
/* eslint-disable no-param-reassign */
import React from 'react';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Layout, Menu, Icon, Input, Drawer, Slider, Skeleton, Divider, Modal,
  Row, Avatar, Button, Steps, notification, Form, List,
} from 'antd';
import { gql } from 'apollo-boost';

import Post from './Post';
import DetailInfo from './detail';
import Contract from './Contract';
import './style.css';
import HeaderFooter from '../../components/LayoutDefault/HeaderFooter/HeaderFooter';

const { Content, Sider } = Layout;
const { SubMenu } = Menu;
const { Search } = Input;

const POSTS = gql`query posts {
  posts {
    _id 
    title
    overview
    hourlyRate
    teacher {
      _id
      fullname
      email
      location {
        address
        city
      }
      phone
      imageURL
      email
    }
    skill{
      _id
      name
      isAproved
    }
    rate
    education{
      name
      timeStart
      timeEnd
      degree
      description
    }
    employeeHistory{
      title
      workPlace
      timeStart
      timeEnd
      description
    }
  }
}
`;

const MESSAGE = gql`
mutation sendMessage($receiverID: ID!, $msg: String!){
  sendMessage(receiverID: $receiverID, msg: $msg)
}
`;

const STUDIED = gql`
query studiedPost {
  studiedPost {
  _id 
  }
}
`;

const FOLLOWED = gql`
query followPost {
  followPost {
    _id
  }
}`;

class Teacher extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      data: [],
      loading: true,
      openPost: null,
      startMoney: 0,
      endMoney: 100,
      totalData: [],
      tagSubject: 'ALL',
      tagLocation: 'ALL',
      openModal: false,
      selectedPost: null,
      openModalWelcome: false,
      steps: [
        {
          id: 1,
          title: 'Đăng ký, Đăng nhập',
          description: '',
          src: 'https://image.flaticon.com/icons/png/512/295/295128.png',
        },
        {
          id: 2,
          title: 'Tỉm kiếm giáo viên',
          description: '',
          src: 'https://image.flaticon.com/icons/png/512/1463/premium/1463514.png',
        },
        {
          id: 3,
          title: 'Ký kết hợp đông',
          description: '',
          src: 'https://image.flaticon.com/icons/png/512/178/178150.png',
        },
        {
          id: 4,
          title: 'Thanh toán, thực thi',
          description: '',
          src: 'https://image.flaticon.com/icons/png/512/1046/premium/1046444.png',
        },
      ],
      openModalChat: false,
      chatID: '',
      currentMenu: 'listMenu',
      menuData: [],
      listSkills: [],
      listCities: [],
    };

    this.handleOpenDrawer = this.handleOpenDrawer.bind(this);
    this.handleCloseDrawer = this.handleCloseDrawer.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
    this.handleTagSubjectClick = this.handleTagSubjectClick.bind(this);
    this.handleTagLocationClick = this.handleTagLocationClick.bind(this);
    this.handleTagMoneyChange = this.handleTagMoneyChange.bind(this);
  }

  componentWillMount() {
    const { client } = this.props;
    const { type, id } = this.props.match.params;
    client.watchQuery({
      query: POSTS,
      fetchPolicy: 'network-only',
    }).subscribe({
      next: ({ data }) => {
        if (data) {
          this.handleSetSkills(data.posts);
          this.setState({
            data: data.posts,
            totalData: data.posts,
          }, () => {
            if (type && type === 'PostID' && id) this.handleOpenDrawer(id);
            this.setState({ loading: false });
            this.handleFilter();
          });
        }
      },
      error: (e) => console.error(e),
    });
  }

  handleSetSkills = (data) => {
    const listSkills = [];
    const listCities = [];
    data.forEach((element) => {
      element.skill.forEach((el) => {
        if (el.isAproved && !listSkills.includes(el)) {
          listSkills.push(el);
        }
      });
      if (element.teacher && element.teacher.location && element.teacher.location.city && !listCities.includes(element.teacher.location.city)) listCities.push(element.teacher.location.city);
    });
    this.setState({ listSkills, listCities }, () => {
      const { type, id } = this.props.match.params;
      console.log(listSkills);
      if (type && type === 'TagID' && id) {
        listSkills.forEach((el) => {
          if (el._id === id) {
            this.setState({
              tagSubject: id,
            }, () => {
              this.handleFilter();
            });
          }
        });
      }
    });
  }

  handleTagSubjectClick(e) {
    this.setState({
      tagSubject: e.key,
    }, () => {
      this.handleFilter();
    });
  }

  handleTagLocationClick(e) {
    this.setState({
      tagLocation: e.key,
    }, () => {
      this.handleFilter();
    });
  }

  handleTagMoneyChange(value) {
    this.setState({
      startMoney: value[0],
      endMoney: value[1],
    }, () => {
      this.handleFilter();
    });
  }

  handleFilter() {
    const { tagLocation, tagSubject, startMoney, endMoney, totalData } = this.state;
    if (tagSubject === 'ALL' && tagLocation === 'ALL' && startMoney === 0 && endMoney === 100) {
      this.setState({
        data: totalData.slice(0),
      });
    } else {
      const newData = [];
      totalData.forEach((el) => {
        const money = parseInt(el.hourlyRate, 0) / 1000;
        if (tagSubject !== 'ALL' && tagLocation !== 'ALL' && (startMoney !== 0 || endMoney !== 100)) {
          el.skill.forEach((sk) => {
            if (sk._id === tagSubject && tagLocation === el.teacher.location.city && money >= startMoney && money <= endMoney) {
              if (!newData.includes(el)) newData.push(el);
            }
          });
        } else if (tagLocation !== 'ALL' && (startMoney !== 0 || endMoney !== 100)) {
          if (el.teacher.location.city === tagLocation && money >= startMoney && money <= endMoney) {
            if (!newData.includes(el)) newData.push(el);
          }
        } else if (tagSubject !== 'ALL' && (startMoney !== 0 || endMoney !== 100)) {
          el.skill.forEach((sk) => {
            if (sk._id === tagSubject && money >= startMoney && money <= endMoney) {
              if (!newData.includes(el)) newData.push(el);
            }
          });
        } else if (tagSubject !== 'ALL' && tagLocation !== 'ALL') {
          el.skill.forEach((sk) => {
            if (sk._id === tagSubject && el.teacher.location.city === tagLocation) {
              if (!newData.includes(el)) newData.push(el);
            }
          });
        } else if (tagSubject !== 'ALL') {
          el.skill.forEach((sk) => {
            if (sk._id === tagSubject) {
              if (!newData.includes(el)) newData.push(el);
            }
          });
        } else if (tagLocation !== 'ALL') {
          if (el.teacher.location.city === tagLocation) {
            if (!newData.includes(el)) newData.push(el);
          }
        } else if ((money >= startMoney && endMoney === 100) || (money >= startMoney && money <= endMoney)) {
          if (!newData.includes(el)) newData.push(el);
        }
        this.setState({
          data: newData,
        });
      });
    }
  }

  handleOpenDrawer(id1) {
    const { data } = this.state;
    const openPost = data.filter((el) => el._id === id1)[0];

    this.setState({
      visible: true,
      openPost,
    });
  }

  handleCloseDrawer() {
    this.setState({
      visible: false,
    });
  }

  handleShowModal = (id) => {
    if (!this.props.user) {
      this.setState({ openModalWelcome: true });
    } else if (this.props.user.role === 'TEACHER') {
      this.openNotificationWithIcon('error', 'Giáo viên không được thực hiện chức năng này!');
    } else {
      const { data } = this.state;
      const selectedPost = data.filter((el) => el._id === id)[0];

      console.log(selectedPost);
      this.setState({
        selectedPost,
        openModal: true,
      });
    }
  }

  handleCloseModal = () => {
    this.setState({
      openModal: false,
    });
  }

  handleSearch = (value) => {
    if (value.toLowerCase().trim() === '') {
      this.setState({ data: this.state.totalData });
      return;
    }
    this.setState({ data: this.state.totalData, loading: true }, () => {
      const { data } = this.state;
      let temp = data.slice(0);
      temp = temp.filter((item) => item.teacher.fullname.toLowerCase().search(
        value.toLowerCase().trim(),
      ) !== -1);
      this.setState({ data: temp, loading: false });
    });
  }

  openNotificationWithIcon = (type, description) => {
    notification[type]({
      message: 'Thông báo',
      description,
    });
  };

  handleChat = (id) => {
    if (!this.props.user) {
      this.setState({ openModalWelcome: true });
      return;
    }
    this.setState({ openModalChat: true, chatID: id });
  }


  sendMessage = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { message } = values;
        const { chatID } = this.state;
        this.props.client.mutate({
          mutation: MESSAGE,
          variables: {
            receiverID: chatID,
            msg: message,
          },
        }).then(() => {
          this.setState({ openModalChat: false });
          this.openNotificationWithIcon('success', 'Gửi tin nhắn thành công!');
        }).catch(() => {
          this.openNotificationWithIcon('error', 'Gửi tin nhắn thất bại!');
        });
      }
    });
  }

  handleOpenModalWelcome = () => {
    this.setState({ openModalWelcome: true });
  }

  handleClickMenuFilter = (e) => {
    const { totalData } = this.state;
    const result = [];
    this.setState({ currentMenu: e.key, loading: true, tagLocation: 'ALL', tagSubject: 'ALL', startMoney: 0, endMoney: 100 }, () => {
      if (e.key === 'listMenu') {
        this.props.client.watchQuery({
          query: POSTS,
          fetchPolicy: 'network-only',
        }).subscribe({
          next: ({ data }) => {
            if (data) {
              this.handleSetSkills(data.posts);
              this.setState({
                data: data.posts,
                totalData: data.posts,
                loading: false,
              });
            }
          },
          error: (e1) => console.error(e1),
        });
      } else if (e.key === 'followedMenu') {
        if (!this.props.user || (this.props.user && this.props.user.role === 'TEACHER')) {
          this.handleSetSkills([]);
          this.setState({ data: [], loading: false });
          return;
        }
        this.props.client.watchQuery({
          query: FOLLOWED,
          fetchPolicy: 'network-only',
        }).subscribe({
          next: (res) => {
            if (res.data) {
              res.data.followPost.forEach((el) => {
                for (let i = 0; i < totalData.length; i++) {
                  if (totalData[i]._id === el._id && result.filter((el1) => (el1._id === totalData[i]._id)).length === 0) {
                    result.push(totalData[i]);
                  }
                }
              });
              this.handleSetSkills(result);
              this.setState({ data: result, loading: false });
            }
          },
          error: (e1) => console.error(e1),
        });
      } else if (e.key === 'studiedMenu') {
        if (!this.props.user || (this.props.user && this.props.user.role === 'TEACHER')) {
          this.handleSetSkills([]);
          this.setState({ data: [], loading: false });
          return;
        }
        this.props.client.watchQuery({
          query: STUDIED,
          fetchPolicy: 'network-only',
        }).subscribe({
          next: (res) => {
            if (res.data) {
              res.data.studiedPost.forEach((el) => {
                for (let i = 0; i < totalData.length; i++) {
                  if (totalData[i].teacher._id === el._id && result.filter((el1) => (el1._id === totalData[i]._id)).length === 0) {
                    result.push(totalData[i]);
                  }
                }
              });
              this.handleSetSkills(result);
              this.setState({ data: result, loading: false });
            }
          },
          error: (e1) => console.error(e1),
        });
      }
    });
  };

  render() {
    const { data, visible, openPost, loading, startMoney, endMoney, openModal, selectedPost, openModalWelcome, steps, tagSubject, openModalChat, listCities, listSkills } = this.state;
    const { getFieldDecorator } = this.props.form;

    const marks = {
      0: '0',
      50: '50K',
      100: {
        style: {
          color: '#f50',
        },
        label: <strong>>100k</strong>,
      },
    };

    const Posts = ((post) => (post.teacher ? <Post
      key={post._id}
      handleOpenDrawer={this.handleOpenDrawer}
      id={post._id}
      title={post.title}
      overview={post.overview}
      teacher={post.teacher}
      skill={post.skill}
      rate={post.rate.reduce((total, items) => (total += items), 0) / post.rate.length}
      hourlyRate={post.hourlyRate}
      handleOpenModal={this.handleShowModal}
      handleChat={this.handleChat}
      handleOpenModalWelcome={this.handleOpenModalWelcome}
    >
    </Post> : null));
    const a = [0, 1, 2, 3, 4];
    const loadSeketon = (a.map((index) => (
      <div key={index}>
        <Skeleton active avatar paragraph={{ rows: 3 }} />
        <Divider></Divider>
      </div>
    )));
    return (
      <HeaderFooter>
        <div className="Student">
          <Layout className="container">
            <Sider width={250} style={{ background: '#fff' }}>
              <h3 style={{ marginLeft: 25, marginTop: 10 }}>Bộ Lọc</h3>
              <Menu mode="inline" defaultSelectedKeys={tagSubject} selectedKeys={tagSubject} defaultOpenKeys={['sub1']} onClick={this.handleTagSubjectClick}>
                <SubMenu key="sub1"
                  title={<span> <Icon type="user" />Môn học</span>}>
                  <Menu.Item key="ALL">Tất cả</Menu.Item>
                  {!loading && listSkills.map((el) => (<Menu.Item key={el._id}>{el.name}</Menu.Item>))}
                </SubMenu>
              </Menu>
              <Menu mode="inline" defaultSelectedKeys={['ALL']} defaultOpenKeys={['sub1']} onClick={this.handleTagLocationClick}>
                <SubMenu key="sub1"
                  title={<span><Icon type="laptop" />Vị trí</span>}>
                  <Menu.Item key="ALL">Tất cả</Menu.Item>
                  {!loading && listCities.map((el) => (<Menu.Item key={el}>{el}</Menu.Item>))}
                </SubMenu>
              </Menu>
              <Menu mode="inline" defaultOpenKeys={['sub1']} style={{ height: '100%' }} selectable={false}>
                <SubMenu key="sub1"
                  title={<span><Icon type="dollar" />Giá tiền</span>}>
                  <Menu.Item style={{ marginLeft: 'unset', height: '120%' }} key="0">
                    <Slider range marks={marks} defaultValue={[startMoney, endMoney]} onAfterChange={this.handleTagMoneyChange} />
                  </Menu.Item>
                </SubMenu>
              </Menu>
            </Sider>
            <Content style={{ padding: '0 24px', minHeight: 500 }}>
              <Menu onClick={this.handleClickMenuFilter} selectedKeys={[this.state.currentMenu]} mode="horizontal">
                <Menu.Item key="listMenu">
                  <Icon type="mail" />
                  Danh sách
                </Menu.Item>
                <Menu.Item key="studiedMenu">
                  <Icon type="appstore" />
                  Đã học
                </Menu.Item>
                <Menu.Item key="followedMenu">
                  <Icon type="appstore" />
                  Theo dõi
                </Menu.Item>
              </Menu>
              <Row>
                <Search className="search-bar" placeholder="Tìm kiếm theo tên giáo viên" size="large" onSearch={this.handleSearch} />
                {loading ? loadSeketon : <List itemLayout="vertical" size="large"
                  pagination={{
                    onChange: (page) => {
                      console.log(page);
                    },
                    pageSize: 10,
                  }}
                  dataSource={data}
                  renderItem={(post) => (
                    <List.Item key={post._id} >
                      {Posts(post)}
                    </List.Item>
                  )}
                />}
              </Row>
            </Content>
          </Layout>
          <Drawer
            width={'50%'}
            className="myDrawer"
            title="Detail Profile"
            placement="right"
            onClose={this.handleCloseDrawer}
            visible={visible}
          >
            {visible && openPost !== null && <DetailInfo
              handleOpenDrawer={this.handleOpenDrawer}
              title={openPost.title}
              overview={openPost.overview}
              teacher={openPost.teacher}
              skill={openPost.skill}
              // eslint-disable-next-line no-return-assign
              rate={openPost.rate.reduce((total, items) => (total += items), 0) / openPost.rate.length}
              education={openPost.education}
              employee_history={openPost.employeeHistory}
              hourlyRate={openPost.hourlyRate}
              id={openPost._id}
              handleCloseDrawer={this.handleCloseDrawer}
              handleOpenModal={this.handleShowModal}
            >
            </DetailInfo>}
          </Drawer>
          {!loading && <Modal
            title="Tạo hợp đồng"
            visible={openModal}
            onOk={this.handleCloseModal}
            confirmLoading={loading}
            onCancel={this.handleCloseModal}
            width={'60%'}
            footer={
              <div style={{ color: 'gray', textAlign: 'center' }}>© Bản quyền thuộc về UberTutor</div>
            }
          >
            <div >
              {openModal && selectedPost !== null && <Contract handleOpenDrawer={this.handleOpenDrawer}
                title={selectedPost.title}
                overview={selectedPost.overview}
                teacher={selectedPost.teacher}
                skill={selectedPost.skill}
                rate={selectedPost.rate.reduce((total, items) => (total += items), 0) / selectedPost.rate.length}
                education={selectedPost.education}
                employee_history={selectedPost.employeeHistory}
                hourlyRate={selectedPost.hourlyRate}
                handleCloseModal={this.handleCloseModal}
              />
              }
            </div>
          </Modal>}
          {!loading && <Modal
            title="Chào mừng bạn đến với Uber Tutor"
            visible={openModalWelcome}
            confirmLoading={loading}
            onCancel={() => { this.setState({ openModalWelcome: false }); }}
            width={'60%'}
            footer={
              <Button type="ghost" style={{ color: 'white', fontSize: 18, textAlign: 'center', width: '100%', background: 'green', height: 40 }}
                onClick={() => { this.props.history.push('/'); }}>Đăng ký/Đăng Nhập</Button>
            }
          >
            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', textAlign: 'center' }}>
              <Row>
                <Avatar src="https://i.imgur.com/43iwhPV.png" shape="square" size={120}></Avatar>
              </Row>
              <Row>
                <h2>Để có thể sử dụng tính năng này với các giáo viên, bạn cần tạo một tài khoản hoặc đăng nhập bằng chính tài khoản của bạn!</h2>
              </Row>
              <Divider></Divider>
              <Row>
                <Steps onChange={this.onChange}>
                  {steps.map((el) => (
                    <Steps.Step status="process" key={el.id} title={el.title} description={<div style={{ textAlign: 'center' }}>
                      <Avatar size={100} src={el.src}></Avatar>
                      <h3>{el.description}</h3>
                    </div>} />
                  ))}
                </Steps>
              </Row>

            </div>
          </Modal>}
          {!loading && <Modal
            title="Gửi tin nhắn"
            visible={openModalChat}
            confirmLoading={loading}
            onCancel={() => { this.setState({ openModalChat: false }); }}
            width={'60%'}
            footer={
              <div style={{ width: '100%', textAlign: 'center' }}>Bản quyền thuộc về Uber Tutor</div>
            }
          >
            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', textAlign: 'center' }}>
              <Form style={{ width: '100%' }} onSubmit={this.sendMessage}>
                <Form.Item>
                  {getFieldDecorator('message', {
                    rules: [{ required: true, message: 'Vui lòng nhập nội dung!' }],
                  })(
                    <Input.TextArea rows={8} />,
                  )}
                </Form.Item>
                <Button type="ghost" style={{ color: 'white', fontSize: 18, textAlign: 'center', width: '100%', background: 'green', height: 40 }} htmlType="submit">Gửi tin nhắn</Button>
              </Form>
            </div>
          </Modal>}
        </div>
      </HeaderFooter>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
});


export default Form.create()(withApollo(withRouter(connect(mapStateToProps, null)(Teacher))));
