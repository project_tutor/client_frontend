import {
  notification,
  Drawer,
  Form,
  Button, Col, Row, Input, DatePicker, Icon, Skeleton,
  List,
} from 'antd';
import React, { useState } from 'react';

const moment = require('moment');

const DrawerForm = (props) => {
  const [visible, setVisible] = useState(false);
  const [index, setIndex] = useState(-1);
  const [edit, setEdit] = useState(false);
  const [DateX, setDate] = useState([]);
  const {
    education,
    setEducation,
    number,
    setNumber,
  } = props;
  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };
  const handleSubmit = (events) => {
    events.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        onClose();
        console.log('Received values of form: ', values);
        if (edit === true) {
          const {
            name,
            degree,
            description,
          } = values;
          education[index].name = name;
          education[index].degree = degree;
          education[index].description = description;
          const ss = moment(values.dateTime[0]).format('DD-MM-YYYY');
          const ss2 = moment(ss, 'DD-MM-YYYY');
          const ss1 = moment(values.dateTime[1]).format('DD-MM-YYYY');
          const ss21 = moment(ss1, 'DD-MM-YYYY');
          const arr = [ss2, ss21];
          setDate(arr);
          // eslint-disable-next-line prefer-destructuring
          education[index].timeStart = ss;
          // eslint-disable-next-line prefer-destructuring
          education[index].timeEnd = ss1;
          setEdit(false);
          setIndex(-1);
          notification.success({
            message: 'Edit success',
          });
        } else {
          const ss = moment(values.dateTime[0]).format('DD-MM-YYYY');
          const ss2 = moment(ss, 'DD-MM-YYYY');
          const ss1 = moment(values.dateTime[1]).format('DD-MM-YYYY');
          const ss21 = moment(ss1, 'DD-MM-YYYY');
          const arr = [ss2, ss21];
          setDate(arr);
          education.push({
            name: values.name,
            degree: values.degree,
            description: values.description,
            timeStart: ss.toString(),
            timeEnd: ss1.toString(),

          });
          notification.success({
            message: 'Add success',
          });
        }
        setEducation(education);
        props.form.resetFields();
      }
    });
  };
  const { getFieldDecorator } = props.form;
  const handleEdit = (id) => {
    setEdit(true);
    setIndex(id);
    showDrawer();
  };
  const handleBack = () => {
    setEducation(education);
    setNumber(number - 1);
  };
  const handleNext = () => {
    if (education.length < 1) {
      notification.error({
        message: 'Nhập ít nhất 1 hồ sơ',
      });
    } else {
      setEducation(education);
      setNumber(number + 1);
    }
  };
  console.log('====================================');
  console.log(education);
  console.log('====================================');
  const handleRemove = (id) => {
    const list = education.filter((item, idx) => idx !== id);
    setEducation(list);
    notification.success({
      message: 'Remove success',
    });
  };
  return (
    <div>

      {education.map((value, idx) => (
        <List.Item key={idx}
          actions={[<Button type="primary" onClick={() => handleEdit(idx)} shape="circle" icon="edit" />, <Button onClick={() => handleRemove(idx)} type="primary" shape="circle" icon="delete" />]}
        >
          <Skeleton avatar title={false} loading={false} active>
            <List.Item.Meta
              title={<span style={{ fontWeight: 'bold' }} >{value.name}</span>}
              description={value.degree}
            />
            <div>{value.timeStart} - {value.timeEnd} </div>
          </Skeleton>
        </List.Item>
      ))}

      <Button type="primary" onClick={showDrawer}>
        <Icon type="plus" /> Thêm hồ sơ
        </Button>
      <Drawer
        title="Add education"
        width={720}
        onClose={onClose}
        visible={visible}
        bodyStyle={{ paddingBottom: 80 }}
      >
        <Form layout="vertical" hideRequiredMark>
          <Row gutter={16}>
            <Col >
              <Form.Item label="Trường">
                {getFieldDecorator('name', {

                  initialValue: edit ? education[index].name : null,
                  rules: [{ required: true, message: 'Vui lòng nhập tên trường' }],
                })(<Input maxLength='40' placeholder="Vui lòng nhập tên trường" />)}
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col >
              <Form.Item label="Degree">
                {getFieldDecorator('degree', {
                  initialValue: edit ? education[index].degree : null,
                  rules: [{ required: true, message: 'Vui lòng nhập tên bằng cấp' }],
                })(
                  <Input maxLength='40' placeholder="Vui lòng nhập tên bằng cấp" />,
                )}
              </Form.Item>
            </Col>

          </Row>
          <Row gutter={16}>
            <Col >
              <Form.Item label="Time Start">
                {getFieldDecorator('dateTime', {
                  initialValue: edit ? DateX : null,
                  rules: [{ required: true, message: 'Vui lòng nhập thời gian' }],
                })(
                  <DatePicker.RangePicker
                    format="YYYY-MM"

                    style={{ width: '100%' }}
                    getPopupContainer={(trigger) => trigger.parentNode}
                  />,
                )}
              </Form.Item>
            </Col>

          </Row>
          <Row gutter={16}>
            <Col span={24}>
              <Form.Item label="Description">
                {getFieldDecorator('description', {
                  initialValue: edit ? education[index].description : null,
                  rules: [
                    {
                      required: true,
                      message: 'please enter description',
                    },
                  ],
                })(<Input.TextArea rows={4} placeholder="please enter url description" />)}
              </Form.Item>
            </Col>
          </Row>
        </Form>
        <div
          style={{
            position: 'absolute',
            right: 0,
            bottom: 0,
            width: '100%',
            borderTop: '1px solid #e9e9e9',
            padding: '10px 16px',
            background: '#fff',
            textAlign: 'right',
          }}
        >
          <Button onClick={onClose} style={{ marginRight: 8 }}>
            Hủy
            </Button>
          <Button onClick={handleSubmit} type="primary">
            Hoàn tất
            </Button>
        </div>
      </Drawer>
      <div className="parent">
        <div className="child">
          <div> <Button type="danger" onClick={handleNext} >Tiếp theo</Button></div>
        </div>
        <Button type="link" onClick={handleBack}>Trở về</Button>
      </div>
    </div>
  );
};

const App = Form.create()(DrawerForm);
export default App;
