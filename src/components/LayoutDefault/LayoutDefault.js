import React from 'react';
import { useHistory, useRouteMatch, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Layout,
  Menu,
  Avatar,
  Dropdown,
  Icon,
  Col,
  Form,
  Input,
  Button,
  Row,
  Divider,
} from 'antd';
import * as actions from '../../tools/redux/actions';
import './index.css';

const url_backend = process.env.REACT_APP_URL_BACKEND_API ? 'https://tutor-client-backend.herokuapp.com' : 'http://localhost:3001';

const LayoutDefault = (props) => {
  const { children, handleSaveUser, user } = props;
  const { Header, Content, Footer } = Layout;
  const history = useHistory();
  const match = useRouteMatch();
  const { path } = match;
  const arrPath = path.split('/');
  const { getFieldDecorator } = props.form;
  const handleMenuClick = (e) => {
    window.scrollTo(0, 0);
    switch (e.key) {
      case 'logout':
        localStorage.removeItem('token');
        handleSaveUser(null);
        history.push('/login');
        break;
      case 'login':
        history.push('/login');
        break;
      case 'profile':
        history.push('/profile');
        break;
      default:
        break;
    }
  };

  const menuWhenHoverUser = (
    <Menu onClick={handleMenuClick}>
      <Menu.Item key="profile">
        <Icon type="user" />
        Thông tin cá nhân
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="logout">
        <Icon type="logout" />
        Đăng xuất
      </Menu.Item>
    </Menu>
  );

  return (
    <>
      <Layout>
        <Header style={{ position: 'fixed', zIndex: 999, width: '100%' }}>
          {user ? (<div className="logo">
            <Dropdown overlay={menuWhenHoverUser} >
              <div className="ant-dropdown-link" style={{ float: 'right' }}>
                <Avatar src={user.imageURL ? url_backend + user.imageURL : ''} shape="square" icon="user" />
              </div>
            </Dropdown>
            {/* <div style={{ float: 'right' }}>
              <Badge dot style={{ marginRight: '10px' }}>
                <Button onClick={() => history.push('/chat')} type="primary" shape="circle" icon="message" style={{ marginRight: '10px' }} />
              </Badge>
            </div> */}
            <div style={{ float: 'right', color: '#85BB65', fontWeight: 'bold', marginRight: '10px' }}>
              {user.money ? `${user.money.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')} VND` : ''}
            </div>
          </div>) : null
          }
          {/* <div className="logo">
            <div className="ant-dropdown-link" style={{ float: 'right', marginRight: 20 }}>
              <Badge dot>
                <Avatar onClick={() => console.log(123)} style={{ backgroundColor: '#1890FF', cursor: 'pointer' }} shape="square" icon="message" />
              </Badge>
            </div>
          </div> */}
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={[`/${arrPath[1]}`]}
            style={{ lineHeight: '64px' }}
            onClick={(e) => {
              window.scrollTo(0, 0);
              history.push(e.key);
            }}
          >
            <Menu.Item key="/">Trang chủ</Menu.Item>
            <Menu.Item key="/teacher">Giáo viên</Menu.Item>
            {user ? <Menu.Item key="/chat">Tin nhắn</Menu.Item> : null}
            <Menu.Item key="/about">Về chúng tôi</Menu.Item>
            {!user ? <Menu.Item key="/login" style={{ float: 'right' }}>Đăng nhập</Menu.Item> : null}
          </Menu>
        </Header>
        <Content style={{ padding: '0 50px', marginTop: 88 }}>
          {children}
        </Content>
        <Footer style={{ textAlign: 'center', padding: 'unset', zIndex: 999 }}>
          <div style={{ background: '#585656', height: 430, color: 'white', padding: 80 }}>
            <Row>
              <Col span={6} style={{ textAlign: 'left' }}>
                <p style={{ fontSize: 30, color: 'white' }}>UBER TUTOR</p>
                <div style={{ marginBottom: 20 }}>
                  <Icon style={{ fontSize: '25px', marginRight: 20 }} type="facebook" />
                  <Icon style={{ fontSize: '25px', marginRight: 20 }} type="google" />
                  <Icon style={{ fontSize: '25px', marginRight: 20 }} type="youtube" />
                  <Icon style={{ fontSize: '25px', marginRight: 20 }} type="twitter" />
                </div>
                <p style={{ fontSize: 25, color: 'orange' }}>Liên lạc</p>
                <div>
                  <Icon type="phone" />
                  <p style={{ fontSize: 20, color: 'white', marginLeft: 10, display: 'inline' }}>+84 962 074 802</p>
                </div>
                <div>
                  <Icon type="mail" />
                  <p style={{ fontSize: 20, color: 'white', marginLeft: 10, display: 'inline' }}>admin@ubertutor.com</p>
                </div>
              </Col>
              <Col span={6} style={{ textAlign: 'left' }}>
                <p style={{ fontSize: 25, color: 'orange' }}>Thông tin</p>
                <Link style={{ fontSize: 20, color: 'white' }} to="/about"> <p>Về chúng tôi</p> </Link>
                <Link style={{ fontSize: 20, color: 'white' }} to="/about"> <p>FAQ</p> </Link>
                <Link style={{ fontSize: 20, color: 'white' }} to="/about"> <p>Thông tin thêm</p> </Link>
                <Link style={{ fontSize: 20, color: 'white' }} to="/about"> <p>Công nghệ</p> </Link>
              </Col>
              <Col span={6} style={{ textAlign: 'left' }}>
                <p style={{ fontSize: 25, color: 'orange' }}>Đường dây nóng</p>
                <p style={{ fontSize: 20, color: 'white' }}>Dịch vụ</p>
                <p style={{ fontSize: 20, color: 'white' }}>Hỗ trợ</p>
                <p style={{ fontSize: 20, color: 'white' }}>Điều khoản và điều kiện</p>
                <p style={{ fontSize: 20, color: 'white' }}>Các chính sách</p>
              </Col>
              <Col span={6} style={{ textAlign: 'left' }}>
                <div style={{ marginBottom: 20 }}>
                  <Icon style={{ fontSize: '25px', marginRight: 10 }} type="mail" />
                  <p style={{ fontSize: 20, color: 'white', marginLeft: 10, display: 'inline' }}>Nhận các thông tin liên quan đến Uber Tutor</p>
                </div>
                <Form>
                  <Form.Item>
                    {getFieldDecorator('email', {
                      rules: [
                        {
                          type: 'email',
                          message: 'The input is not valid E-mail!',
                        },
                        {
                          required: true,
                          message: 'Please input your E-mail!',
                        },
                      ],
                    })(<Input placeholder="Nhập email của bạn" />)}
                  </Form.Item>
                  <Button style={{ background: 'red', color: 'white' }}>Đăng ký</Button>
                </Form>
              </Col>
            </Row>
            <Divider></Divider>
            <Row>
              Uber Tutor ©2019. All Rights Reveived.
            </Row>
          </div>
        </Footer>
      </Layout>
    </>);
};

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
  handleSaveUser: (user) => {
    dispatch(actions.saveUser(user));
  },
});


export default Form.create()(connect(mapStateToProps, mapDispatchToProps)(LayoutDefault));
