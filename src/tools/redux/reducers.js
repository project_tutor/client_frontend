import { combineReducers } from 'redux';
import user from './store/user';

const reducers = combineReducers({
  user,
});

export default reducers;
