// eslint-disable
import React, { useState, useEffect } from 'react';
import { Card, List, Avatar, Row, Col, Input, Spin } from 'antd';
import { useQuery, useMutation, useSubscription } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { useHistory } from 'react-router-dom';

import LayoutHeaderFooter from '../../components/LayoutDefault/HeaderFooter/HeaderFooter';
import './style.scss';

const MY_ROOM_CHAT = gql`
 query{
  myRoomChat{
    _id
    member{
      _id
      fullname
      imageURL
    }
    allMessage{
      sender{
        _id
        email
        fullname
      }
      msg
      createAt
    }
  }
}
`;

const SEND_MESSAGE = gql`
 mutation SendMessage($receiverID: ID!, $msg: String!){
  sendMessage(receiverID: $receiverID, msg: $msg)
}
`;

const ON_MESSAGE = gql`
subscription {
  onMessages {
    roomChatID
    msg
    sender {
      _id
      fullname
      email
    }
  }
}
`;


const url_backend = process.env.REACT_APP_URL_BACKEND_API ? 'https://tutor-client-backend.herokuapp.com' : 'http://localhost:3001';

const { Search } = Input;

function Chat() {
  const [messageChat, setMessageChat] = useState('');
  const { loading, error, data: dateRoomChat, refetch: refetchDataRoomChat } = useQuery(MY_ROOM_CHAT, { fetchPolicy: 'network-only' });
  const [sendMessage] = useMutation(SEND_MESSAGE);
  const history = useHistory();
  const { loading: loading_on_message } = useSubscription(ON_MESSAGE, { fetchPolicy: 'network-only' });

  useEffect(() => {
    if (document.querySelector('.chat-line')) {
      document.querySelector('.chat-line').scrollTop = document.querySelector('.chat-line').scrollHeight;
    }
  });

  if (loading) return <LayoutHeaderFooter><Spin /></LayoutHeaderFooter>;
  if (error) return <p>Error :(</p>;
  const idSelected = history.location.pathname.substring(history.location.pathname.lastIndexOf('/') + 1);
  const indexIDSelected = dateRoomChat.myRoomChat.findIndex((item) => item.member[0]._id === idSelected);

  if (!loading_on_message) {
    refetchDataRoomChat();
  }

  return (
    <LayoutHeaderFooter>
      <div className='chat'>
        <Card style={{ width: '100%', height: '750px', padding: 0 }}>
          <Row>
            <Col span={6}>
              <Card title="Tin nhắn" style={{ width: '100%' }}>
                <List
                  itemLayout="horizontal"
                  dataSource={dateRoomChat.myRoomChat}
                  renderItem={(item) => (
                    <List.Item
                      onClick={() => history.push(`/chat/${item.member[0]._id}`)}
                      style={{
                        cursor: 'pointer', background: `${item.member[0]._id === idSelected ? '#F2F2F2' : ''}`,
                      }}
                    >
                      <List.Item.Meta
                        avatar={
                          <span style={{ paddingLeft: 10 }}>
                            <Avatar src={url_backend + item.member[0].imageURL} />
                          </span>
                        }
                        title={<div style={{ lineHeight: '32px' }}>{item.member[0].fullname}</div>}
                      />
                    </List.Item>
                  )}
                />
              </Card>
            </Col>
            <Col span={18}>
              <Card title="Hộp thoại" style={{ position: 'relative', width: '100%', height: '750px' }} >
                <div className="chat-line">
                  {indexIDSelected !== -1 && dateRoomChat.myRoomChat[indexIDSelected].allMessage.map((item, index) => <div key={index} className={`${idSelected === item.sender._id ? 'not-me' : 'me'}`}>
                      <span>
                        {item.msg}
                      </span>
                    </div>)}
                  {/* {!loading_on_message &&
                    <div className='not-me'>
                      <span>
                        {subDataMessage.onMessages.msg}
                      </span>
                    </div>
                  } */}
                </div>
                <div>
                  <Search
                    disabled={indexIDSelected === -1}
                    enterButton="Gửi"
                    size="large"
                    value={messageChat}
                    onChange={(e) => setMessageChat(e.target.value)}
                    onSearch={() => {
                      sendMessage({
                        variables: {
                          receiverID: idSelected,
                          msg: messageChat,
                        },
                        refetchQueries: ['myRoomChat'],
                      }).then(() => {
                        setMessageChat('');
                        refetchDataRoomChat();
                      });
                      refetchDataRoomChat();
                    }}
                    style={{ position: 'absolute', bottom: 0, left: 0, padding: 5 }}
                  />
                </div>
              </Card>
            </Col>
          </Row>
        </Card>
      </div>
    </LayoutHeaderFooter>
  );
}

export default Chat;
