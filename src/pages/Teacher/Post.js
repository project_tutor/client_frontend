import React from 'react';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { gql } from 'apollo-boost';
import {
  Layout, Row, Col, Avatar, Button, Icon, Tag, Rate, Tooltip, notification, Divider,
} from 'antd';

import './style.css';

const { Content } = Layout;

const url_backend = process.env.REACT_APP_URL_BACKEND_API ? 'https://tutor-client-backend.herokuapp.com' : 'http://localhost:3001';

const FOLLOW = gql`
mutation followingPost($input: ID!) {
  followingPost(postId: $input)
}
`;

const CANCELFOLLOW = gql`
mutation cancelFollowPost($input: ID!) {
     cancelFollowPost(postId: $input)
}
`;

const FOLLOWED = gql`
query followPost {
  followPost {
    _id
  }
}`;

class Post extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      goToRegister: false,
      loading: false,
      changeBG: false,
      isFollowed: false,
    };
    this.handleMouseEnter = this.handleMouseEnter.bind(this);
    this.handleMouseLeave = this.handleMouseLeave.bind(this);
    this.handleOpenDrawer = this.handleOpenDrawer.bind(this);
    this.handleRegisterTeacher = this.handleRegisterTeacher.bind(this);
    this.handleFollow = this.handleFollow.bind(this);
    this.handleFollowing = this.handleFollowing.bind(this);
    this.handleCancelFollow = this.handleCancelFollow.bind(this);
  }

  componentWillMount() {
    const { id } = this.props;
    if (this.props.user && this.props.user.role !== 'TEACHER') {
      this.setState({ loading: true }, () => {
        this.props.client.watchQuery({
          query: FOLLOWED,
          fetchPolicy: 'network-only',
        }).subscribe({
          next: ({ data }) => {
            if (data) {
              for (let i = 0; i < data.followPost.length; i++) {
                if (data.followPost[i]._id === id) {
                  this.setState({ isFollowed: true, loading: false });
                  break;
                }
              }
            }
            this.setState({ loading: false });
          },
          error: (e1) => console.error(e1),
        });
      });
    }
  }

  handleMouseEnter() {
    this.setState({
      changeBG: true,
    });
  }

  handleMouseLeave() {
    this.setState({
      changeBG: false,
    });
  }

  handleOpenDrawer() {
    const { handleOpenDrawer, id } = this.props;
    handleOpenDrawer(id);
  }

  handleRegisterTeacher() {
    this.props.handleOpenModal(this.props.id);
  }

  handleFollow() {
    if (this.state.loading) return;
    if (!this.props.user) {
      this.props.handleOpenModalWelcome();
      return;
    }
    if (this.props.user.role === 'TEACHER') {
      this.openNotificationWithIcon('error', 'Giáo viên không được hỗ trợ tính năng này!');
      return;
    }
    if (this.state.isFollowed) {
      this.handleCancelFollow(this.props.id);
    } else {
      this.handleFollowing(this.props.id);
    }
  }

  handleFollowing(id) {
    this.setState({ loading: true }, () => {
      this.props.client.mutate({
        mutation: FOLLOW,
        variables: {
          input: id,
        },
      }).then(() => {
        this.openNotificationWithIcon('success', 'Theo dõi thành công');
        this.setState({ isFollowed: true, loading: false });
      }).catch((err) => {
        this.openNotificationWithIcon('error', err.message.substring(15));
        this.setState({ isFollowed: false, loading: false });
      });
    });
  }

  handleCancelFollow(id) {
    this.setState({ loading: true }, () => {
      this.props.client.mutate({
        mutation: CANCELFOLLOW,
        variables: {
          input: id,
        },
      }).then(() => {
        this.openNotificationWithIcon('success', 'Bỏ theo dõi thành công');
        this.setState({ isFollowed: false, loading: false });
      }).catch((err) => {
        this.openNotificationWithIcon('error', err.message.substring(15));
        this.setState({ isFollowed: true, loading: false });
      });
    });
  }

  openNotificationWithIcon = (type, description) => {
    notification[type]({
      message: 'Thông báo',
      description,
    });
  };

  handleChat = () => {
    this.props.handleChat(this.props.teacher._id);
  }


  render() {
    const { title, teacher, hourlyRate, overview, skill, rate } = this.props;
    const { fullname, location, phone, imageURL } = teacher;
    const { changeBG, isFollowed, loading } = this.state;
    return (
        <Layout style={{ minWidth: 700 }} className={changeBG ? 'post-container changeBG' : 'post-container'}>
            <div id="a" className='hihi' onClick={this.handleOpenDrawer} onMouseEnter={this.handleMouseEnter} onMouseLeave={this.handleMouseLeave}></div>
            <Content id="b" style={{ padding: '5 5px', minHeight: 180 }} >
              <Row>
                <Col span={2}>
                    <Avatar src={url_backend + imageURL} style={{ backgroundColor: this.state.color, verticalAlign: 'middle' }} size="large"></Avatar>
                </Col>
                <Col span={4}>
                    <p style={{ margin: 'unset', color: 'green' }} >{fullname}</p>
                    <p style={{ margin: 'unset' }} >Giáo viên</p>
                </Col>
                <Col span={4} offset={4} style={{ minWidth: 150 }}><Rate disabled defaultValue={rate}/></Col>
                <Col span={1} style={{ minWidth: 50, float: 'right' }}>
                  <Tooltip placement="right" title={isFollowed ? 'Huỷ theo dõi' : 'Theo dõi' }>
                    <Button style={{ background: isFollowed ? 'green' : 'red' }} className="btn" ghost={true} type="primary" icon={isFollowed ? 'check' : 'plus'} shape="circle"
                      onMouseEnter={this.handleMouseEnter} onMouseLeave={this.handleMouseLeave} onClick={this.handleFollow} loading={loading}>
                    </Button>
                  </Tooltip>
                </Col>
                <Col span={1} style={{ minWidth: 50, float: 'right' }}>
                  <Tooltip placement="right" title={ 'Nhắn tin'} >
                    <Button style={{ background: isFollowed ? 'green' : 'red' }} className="btn" ghost={true} type="primary" icon={'message'} shape="circle"
                      onMouseEnter={this.handleMouseEnter} onMouseLeave={this.handleMouseLeave} onClick={this.handleChat} disabled={this.props.user && teacher._id === this.props.user._id}>
                    </Button>
                  </Tooltip>
                </Col>
              </Row>
              <Row style={{ margin: 10 }}>
                <Col span={8} >
                  <Icon type="home" />
                  <p style={{ marginLeft: 10, color: 'blue', display: 'inline', minWidth: 320 }} >
                      {`${location.address}, ${location.city}`}</p>
                </Col>
                <Col span={4} offset={2}>
                  <Icon type="phone" />
                  <p style={{ marginLeft: 10, color: 'blue', display: 'inline' }} >{phone}</p>
                </Col>
                <Col span={4} offset={6}>
                  <Icon type="dollar" />
                  <p style={{ marginLeft: 10, color: 'red', display: 'inline' }} >{`${hourlyRate}VNĐ/Giờ`}</p>
                </Col>
              </Row>
              <Divider></Divider>
              <Row style={{ padding: '5 5px', minHeight: 100 }}>
                  <p style={{ fontWeight: 'bold' }}> {title}</p>
                  <p className="dot"> {overview}</p>
              </Row>
              <Row>
                <Col span={16}>
                    <p style={{ color: 'red', display: 'inline', marginRight: 20 }} >Môn học:</p>
                    {skill.map((skills) => (skills.isAproved ? <Tag key={skills._id} color="cyan">{skills.name}</Tag> : null))}
                </Col>
                <Col span={2} offset={4} style={{ minWidth: 50, float: 'right', marginRight: 30 }}>
                    <Button className="btn" type="primary" icon="plus"
                      onMouseEnter={this.handleMouseEnter} onMouseLeave={this.handleMouseLeave} onClick={this.handleRegisterTeacher}>Đăng ký</Button>
                </Col>
              </Row>
            </Content>
        </Layout>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
});

export default withApollo(withRouter(connect(mapStateToProps, null)(Post)));
