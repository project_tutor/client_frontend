/* eslint-disable no-underscore-dangle */
/* eslint-disable indent */
import React, { useState } from 'react';
import {
    Tag, Tooltip, Icon,
    Card, Select,
    Form, notification,
} from 'antd';

const { Option } = Select;
const Tags = (props) => {
    const [inputVisible, setInputVisible] = useState(false);
    const [inputValue, setInputValue] = useState('');

    const { skill, setSkill, dataTags } = props;
    console.log('============= skill =======================');
    console.log(skill);
    console.log(dataTags);
    console.log('====================================');
    const handleClose = (removedTag) => {
        const newTag = skill.filter((tag) => tag !== removedTag);
        console.log(newTag);
        setSkill(newTag);
    };
    const showInput = () => {
        setInputVisible(true);
    };
    const handleInputConfirm = () => {
        setInputVisible(false);
        setInputValue(false);
        setInputValue('');
        console.log('====================================');
        console.log(inputValue);
        console.log('====================================');
    };
    const handleChange = (value) => {
        let value1 = value[0].toLowerCase();
        value1 = `${(value1[0]).toUpperCase()}${value1.substring(1)}`;
        if (value1 && skill.indexOf(value1) === -1) {
            setSkill([...skill, value1]);
        } else {
            notification.error({
                message: 'Kỹ năng đã được có',
            });
        }
        setInputVisible(false);
        setInputValue(false);
        setInputValue('');
    };
    return (
        <div>
            <Card title="Kỹ năng">
                <div>
                    {skill.map((tag) => {
                        const isLongTag = tag.length > 20;
                        const tagElem = (
                            <Tag color="#87d068" key={tag} closable='true' onClose={() => handleClose(tag)}>
                                {isLongTag ? `${tag.slice(0, 20)}...` : tag}
                            </Tag>
                        );
                        return isLongTag ? (
                            <Tooltip title={tag} key={tag}>
                                {tagElem}
                            </Tooltip>
                        ) : (
                                // eslint-disable-next-line indent
                                tagElem
                                // eslint-disable-next-line indent
                            );
                    })}
                    {inputVisible && (
                        <Select mode="tags" style={{ width: '50%' }}
                            autoFocus
                            onBlur={handleInputConfirm}
                            placeholder="Tags Mode" onChange={handleChange}>
                            {dataTags.map((element) => <Option key={element.name}>{element.name}</Option>)}
                        </Select>
                    )}
                    {!inputVisible && (
                        <Tag onClick={showInput} style={{ background: '#fff', borderStyle: 'dashed' }}>
                            <Icon type="plus" /> New Tag
          </Tag>
                    )}
                </div>
            </Card>
        </div >
    );
};


const App = Form.create()(Tags);

export default App;
