import React from 'react';
import { Form, Input, Button, notification } from 'antd';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

const STUDNET_COMPLAINT_CONTRACT = gql`
mutation StudentComplaintContract($contractID: ID!, $studentMessage: String!){
  studentComplaintContract(contractID: $contractID, studentMessage: $studentMessage)
}
`;
const { TextArea } = Input;

function FormComplaint(props) {
  const { form, contractID, refetchDataContract, setVisibaleModal } = props;
  const { getFieldDecorator } = form;
  const [studentComplaintContract] = useMutation(STUDNET_COMPLAINT_CONTRACT);

  const handleSubmit = (e) => {
    e.preventDefault();
    form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        studentComplaintContract({
          variables: {
            contractID,
            studentMessage: values.studentMessage,
          },
        }).then(() => {
          refetchDataContract();
          setVisibaleModal(false);
          notification.success({
            message: 'Bạn đã khiếu nại hợp đồng thành công!',
          });
        });
      }
    });
  };

  return (
    <Form onSubmit={handleSubmit} className="login-form">
      <Form.Item label="Nôi dụng khiếu nại">
        {getFieldDecorator('studentMessage', {
        })(
          <TextArea rows={4} />,
        )}
      </Form.Item>
      <Button type="primary" htmlType="submit" className="login-form-button">
        Xác nhận
      </Button>
    </Form>
  );
}

const WrapperFormComplaint = Form.create({ name: 'FormComplaint' })(FormComplaint);

export default WrapperFormComplaint;
