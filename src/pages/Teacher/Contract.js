import React from 'react';
import { connect } from 'react-redux';
import { Layout, Row, Col, Avatar, Select, Icon, Input,
  Divider, Timeline, Form, Button, InputNumber, Checkbox, message, notification } from 'antd';
import { gql } from 'apollo-boost';
import { withApollo } from 'react-apollo';

const url_backend = process.env.REACT_APP_URL_BACKEND_API ? 'https://tutor-client-backend.herokuapp.com' : 'http://localhost:3001';

const CONTRACT = gql`mutation registerContract($teacherID: ID!, $input: ContractInput! ) {
  registerContract(teacherID: $teacherID, input: $input)
}
`;

class Contract extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      action: null,
      subject: this.props.skill[0],
      hourly: this.props.hourlyRate,
      hours: 1,
      money: 0,
      changeHourly: true,
      agreement: false,
      loading: false,
    };

    this.handleCreateContract = this.handleCreateContract.bind(this);
    this.handleSubjectChange = this.handleSubjectChange.bind(this);
    this.handleCaCulateMoney = this.handleCaCulateMoney.bind(this);
    this.validateToNextPassword = this.validateToNextPassword.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.openNotificationWithIcon = this.openNotificationWithIcon.bind(this);
  }

  componentDidMount() {
    this.handleCaCulateMoney(this.props.hourlyRate, 'hourly');
    this.props.form.setFieldsValue({
      feeHourly: this.props.hourlyRate,
      timeStudy: this.state.hours,
    });
  }

  handleCreateContract(e) {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        if (!this.state.agreement) {
          message.error('Vui lòng chấp nhận điều khoản của chúng tôi!');
        } else {
          this.setState({ loading: true }, () => {
            const { title, description, feeHourly, timeStudy, skill } = values;
            this.props.client.mutate({
              mutation: CONTRACT,
              variables: {
                teacherID: this.props.teacher._id,
                input: { title, description, feeHourly: `${feeHourly}`, timeStudy, skill },
              },
            }).then((res) => {
              this.openNotificationWithIcon('success', 'Tạo hợp đồng thành công!');
              this.setState({ loading: false });
              this.props.handleCloseModal();
              console.log(res);
            }).catch((err1) => {
              this.openNotificationWithIcon('error', err1.message.substring(15));
              this.setState({ loading: false });
            });
          });
        }
      }
    });
  }

  handleSubjectChange(value) {
    this.setState({
      subject: value,
    });
  }

  openNotificationWithIcon(type, description) {
    notification[type]({
      message: 'Thông báo',
      description,
    });
  }

  handleCaCulateMoney(value, target) {
    const formatter = new Intl.NumberFormat('it-IT', {
      style: 'currency',
      currency: 'VND',
    });
    if (value === null) {
      if (target === 'hours') {
        this.setState({
          hours: 1,
          money: formatter.format(this.state.hourly),
        });
      } else {
        this.setState({
          hourly: 1,
          money: formatter.format(this.state.hours),
        });
      }
    } else if (target === 'hours') {
      this.setState({
        hours: value,
        money: formatter.format(this.state.hourly * value),
      });
    } else {
      this.setState({
        hourly: value,
        money: formatter.format(this.state.hours * value),
      });
    }
  }

  handleToggle() {
    this.setState({
      changeHourly: !this.state.changeHourly,
    });
  }

  validateToNextPassword = (rule, value, callback) => {
    console.log(value);
    callback();
  };

  render() {
    const { teacher, skill, user } = this.props;
    const { fullname, location, phone, imageURL, email } = teacher;
    const { getFieldDecorator } = this.props.form;
    const { money, changeHourly, loading } = this.state;

    return (
      <Layout className="Detail-profile">
        <Form onSubmit={this.handleCreateContract} >
          <Row style={{ display: 'block' }}>
            <Form.Item label={'Tên hợp đồng:'}>
              {getFieldDecorator('title', {
                rules: [
                  {
                    required: true,
                    message: 'Tên hợp đồng không được bỏ trống!',
                  },
                ],
              })(<Input disabled={loading}/>)}
            </Form.Item>
          </Row>
          <Divider style={{}} orientation="center">Thông tin</Divider>
          <Row style={{ background: 'white', overflow: 'hidden', display: 'flex', flexDirection: 'row' }}>
            <Col span={12} >
              <Row ><h1 style={{ color: 'red' }}> Giáo viên</h1></Row>
              <Row style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                <Avatar src={url_backend + imageURL} size={150}></Avatar>
                <h1 style={{ marginTop: 5, color: 'green' }}>{fullname}</h1>
              </Row>
              <Divider orientation="left">Liên hệ</Divider>

              <Row style={{}}>
                <div>
                  <Icon type="home" />
                  <div style={{ marginLeft: 10, padding: 'unset', display: 'inline' }} >{`${location.address}, ${location.city}`} </div>
                </div>
                <div>
                  <Icon type="phone" />
                  <p style={{ marginLeft: 10, display: 'inline' }} >{phone}</p>
                </div>
                <div>
                  <Icon type="mail" />
                  <p style={{ marginLeft: 10, display: 'inline' }} >{email}</p>
                </div>
              </Row>

            </Col>

            <Timeline>
              <Timeline.Item style={{ height: '100%' }}></Timeline.Item>
              <Timeline.Item></Timeline.Item>
            </Timeline>
            <Col span={13}>
              <Row ><h1 style={{ color: 'red' }}> Học sinh</h1></Row>

              <Row style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                <Avatar src={url_backend + user.imageURL} size={150}></Avatar>
                <h1 style={{ marginTop: 5, color: 'green' }}>{user.fullname}</h1>
              </Row>
              <Divider orientation="left">Liên hệ</Divider>

              <Row style={{}}>
                <div>
                  <Icon type="home" />
                  <div style={{ marginLeft: 10, padding: 'unset', display: 'inline' }} >{`${user.location.address}, ${user.location.city}`} </div>
                </div>
                <div>
                  <Icon type="phone" />
                  <p style={{ marginLeft: 10, display: 'inline' }} >{user.phone}</p>
                </div>
                <div>
                  <Icon type="mail" />
                  <p style={{ marginLeft: 10, display: 'inline' }} >{`${user.email}$/hrs`}</p>
                </div>
              </Row>
            </Col>
          </Row>
          <Divider style={{ marginTop: 50 }} orientation="center">Mô tả</Divider>
          <Row style={{ marginTop: 20 }}>
            <h3><b>Mô tả hợp đồng: </b></h3>
            <Form.Item>
              {getFieldDecorator('description', {
                rules: [
                  {
                    required: true,
                    message: 'Mô tả không được bỏ trống!',
                  },
                ],
              })(
                <div style={{ width: '100%', display: 'flex', flexDirection: 'row' }}>
                  <Input.TextArea rows={8} disabled={loading}/>
                </div>,
              )}
            </Form.Item>
          </Row>
          <Row style={{ marginTop: 20 }}>
            <h3><b>Chọn môn học: </b></h3>
            <Form.Item>
              {getFieldDecorator('skill', {
                rules: [
                  {
                    required: true,
                    message: 'Vui lòng chọn môn học!',
                  },
                ],
              })(
                <Select
                  defaultActiveFirstOption
                  style={{ width: '100%' }}
                  onChange={this.handleSubjectChange}
                  placeholder="Chọn môn học"
                  disabled={loading}
                >
                  {skill.map((mSkill) => (<Select.Option key={mSkill._id} value={mSkill._id}>{mSkill.name}</Select.Option>))}
                </Select>,
              )}
            </Form.Item>
          </Row>
          <Divider style={{ marginTop: 50 }} orientation="center">Thanh toán</Divider>
          <Row style={{ marginTop: 20 }}>
            <h3><b>Tiền thuê trên giờ(VNĐ/Giờ): </b></h3>
            <Form.Item>
              {getFieldDecorator('feeHourly', {
              })(
                  <InputNumber
                    formatter={(value) => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                    parser={(value) => value.replace(/\$\s?|(,*)/g, '').replace('K', '')}
                    onChange={(value) => { this.handleCaCulateMoney(value, 'hourly'); }}
                    style={{ width: 100 }}
                    min={1}
                    disabled={changeHourly || loading}
                    className="mInputHourly"
                  />,
              )}
            </Form.Item>
            <div style={{ marginTop: 20 }}>
                    <Button disabled={loading} style={{ width: 100 }} onClick={this.handleToggle} hidden={!changeHourly} type="primary">
                      Thay đổi
                    </Button>
            </div>,

          </Row>
          <Row style={{ marginTop: 20 }}>
            <h3><b>Số Giờ Thuê(Giờ): </b></h3>
            <Form.Item>
              {getFieldDecorator('timeStudy', {
              })(
                  <InputNumber
                    formatter={(value) => `${value}H`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                    parser={(value) => value.replace(/\$\s?|(,*)/g, '').replace('H', '')}
                    onChange={(value) => { this.handleCaCulateMoney(value, 'hours'); }}
                    min={1}
                    disabled={loading}
                  />,
              )}
            </Form.Item>
          </Row>
          <Row>
            <h3 style={{ color: 'red' }}><b>{`Tổng số tiền phải trả: ${money}`}</b></h3>
          </Row>
          <Divider></Divider>
          <Form.Item>
            {getFieldDecorator('agreement', {
              valuePropName: 'checked',
              validator: this.validateToNextPassword,
            })(
              <Checkbox disabled={loading} onChange={(e) => (this.setState({ agreement: e.target.checked }))}>
                Tôi đã đọc và đồng ý với tất cả <a href="/teacher">điều khoản</a> về hợp đồng của UberTutor
              </Checkbox>,
            )}
        </Form.Item>
          <Row style={{ marginTop: 20 }}>
            <Button style={{ background: 'green' }} type="primary" htmlType="submit" loading={loading} >Tạo hợp đồng</Button>
          </Row>
        </Form>
      </Layout>
    );
  }
}

const mapStateToProps = (mState) => ({ user: mState.user });

export default Form.create({ name: 'Contract' })(withApollo(connect(mapStateToProps, null)(Contract)));
