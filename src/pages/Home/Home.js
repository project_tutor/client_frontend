/* eslint-disable no-return-assign */
/* eslint-disable no-param-reassign */
import React from 'react';
import { Layout, Row, Carousel, Divider, Card, Col, Icon, Avatar, Rate, Tag, Steps, Skeleton } from 'antd';
import { gql } from 'apollo-boost';
import './style.css';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import LayoutHeaderFooter from '../../components/LayoutDefault/HeaderFooter/HeaderFooter';
// const Home = () => <LayoutHeaderFooter><h1>Home Page</h1></LayoutHeaderFooter>;

const USERS = gql`
query topTeachers {
  topTeachers {
    _id
    teacher {
      _id
      fullname
      location {
        city
      }
      money
      imageURL
      phone
      email
    }
    rate
    skill {
      _id
      name
    }
  }
}
`;

const TAGS = gql`query topTags {
  topTags {
    _id
    name
  }
}
`;

const url_backend = process.env.REACT_APP_URL_BACKEND_API ? 'https://tutor-client-backend.herokuapp.com' : 'http://localhost:3001';


class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      intro: [{
        id: 1,
        title: 'Chào mừng bạn đến với Uber Tutor!',
        description: '"Nơi kết nối người học và người dạy."',
        src: 'https://images7.alphacoders.com/886/thumb-1920-886684.jpg',
      },
      {
        id: 2,
        title: 'Uber Tutor!',
        description: '"Mang cô giáo đến gần bạn."',
        src: 'https://images7.alphacoders.com/422/thumb-1920-422103.jpg',
      },
      {
        id: 3,
        title: 'Uber Tutor!',
        description: '"Hãy học theo cách của bạn và trả tiền theo hợp đồng của chúng tôi."',
        src: 'https://images6.alphacoders.com/361/thumb-1920-361223.jpg',
      },
      {
        id: 4,
        title: 'Chào mừng bạn đến với Uber Tutor!',
        description: '"Với Uber Tutor, tất cả đều có thể. Khi tất cả đều có thể, việc học sẽ tốt đẹp hơn."',
        src: 'https://images6.alphacoders.com/394/thumb-1920-394159.jpg',
      },
      ],
      steps: [
        {
          id: 1,
          title: 'Đăng ký, đăng nhập',
          description: 'Đăng ký một tài khoản mới hoặc dùng tài khoản của bạn để đăng nhập vào Uber Tutor.',
          src: 'https://image.flaticon.com/icons/png/512/295/295128.png',
        },
        {
          id: 2,
          title: 'Giới thiệu, quảng bá',
          description: 'Tạo một bản tin giới thiệu về các thông tin liên lạc, trình độ học vấn, kinh nghiệm làm việc của bạn đến các học viên.',
          src: 'https://image.flaticon.com/icons/png/512/1463/premium/1463514.png',
        },
        {
          id: 3,
          title: 'Ký kết hợp đồng',
          description: 'Các học viên thấy phù hợp sẽ liên lạc với bạn và tạo ra các hợp đồng về việc dạy học.',
          src: 'https://image.flaticon.com/icons/png/512/178/178150.png',
        },
        {
          id: 4,
          title: 'Thanh toán, thực thi',
          description: 'Sau khi đã đàm phán thì học viên sẽ thanh toán và bạn sẽ bắt đầu công việc của mình.',
          src: 'https://image.flaticon.com/icons/png/512/1046/premium/1046444.png',
        },
      ],
      reviews: [
        {
          id: 1,
          name: 'Trương Lê Việt Danh',
          education: 'Đại học khoa học tự nhiên',
          description: 'Học ở Uber Tutor giúp tiết kiệm được rất nhiều thời gian, công sức để di chuyển',
          src: 'https://scontent.fsgn5-6.fna.fbcdn.net/v/t1.0-9/55788421_836882009980928_8512432323755507712_n.jpg?_nc_cat=106&_nc_ohc=RGKQBQI22FsAQkb0bGp4qR3r6LQxmT6O-TRSQL9bw098uc6AuJSTKfz0w&_nc_ht=scontent.fsgn5-6.fna&oh=1363a9b6506b1f8ac634dc22676f809c&oe=5E6E39AC',
          background: 'https://images5.alphacoders.com/394/thumb-1920-394862.jpg',
          rate: 5,
        },
        {
          id: 2,
          name: 'Phạm Thiên Bảo',
          education: 'Đại học khoa học tự nhiên',
          description: 'Việc thanh toán nhanh, gọn lẹ củng như các hợp đồng minh bạch giúp việc học trở nên an toàn hơn.',
          src: 'https://scontent.fsgn5-6.fna.fbcdn.net/v/t1.0-9/71875491_1389058384590495_2257585714453544960_n.jpg?_nc_cat=109&_nc_ohc=4HEBgxBmJi4AQlxTXPZSqfs05kyWx3E9AVztZKLtyVvqdi-jG2GWPLPPA&_nc_ht=scontent.fsgn5-6.fna&oh=1b081541bc16a6012f18b47a5be1bc49&oe=5E81753B',
          background: 'https://images5.alphacoders.com/394/thumb-1920-394862.jpg',
          rate: 5,
        },
        {
          id: 3,
          name: 'Vũ Tuấn Anh',
          education: 'Đại học khoa học tự nhiên',
          description: 'Số lượng môn học rất đa dạng và phong phú, rất thích hợp đối với bạn thân em. Em sẽ tạo thêm nhiều hợp đồng học trong tương lai.',
          src: 'https://scontent.fsgn5-5.fna.fbcdn.net/v/t1.0-9/78377773_1154376398285297_2895087830457909248_n.jpg?_nc_cat=100&_nc_ohc=ZqAYwqnaTWAAQmOXTBWOIJREQocRHzsLhbV9tzjSSEaPwY-2cEoGnQ2OA&_nc_ht=scontent.fsgn5-5.fna&oh=79cd82f0c95f77161fd681110c4391e0&oe=5E81863F',
          background: 'https://images5.alphacoders.com/394/thumb-1920-394862.jpg',
          rate: 5,
        },
      ],
      subjects: [],
      teachers: [],
      loading: false,
    };

    this.handleClickSubjects = this.handleClickSubjects.bind(this);
    this.handleClickTeacher = this.handleClickTeacher.bind(this);
    this.handleGetTopTeachers = this.handleGetTopTeachers.bind(this);
  }

  componentWillMount() {
    this.setState({ loading: true }, () => {
      this.props.client.query({
        query: TAGS,
      }).then((res) => {
        this.setState({ subjects: res.data.topTags });
        this.props.client.query({
          query: USERS,
        }).then((res1) => {
          this.handleGetTopTeachers(res1.data.topTeachers);
        }).catch((err) => {
          console.log(err);
        });
      });
    });
  }

  handleGetTopTeachers(data) {
    const teachers = [];
    let i = 0;
    data.forEach((el) => {
      teachers.push({ index: i, ...el });
      i++;
    });
    this.setState({ teachers, loading: false });
  }

  handleClickSubjects(value) {
    window.scrollTo(0, 0);
    this.props.history.push(`/teacher/TagID/${value}`);
  }

  handleClickTeacher(value) {
    window.scrollTo(0, 0);
    this.props.history.push(`/teacher/PostID/${value}`);
  }

  render() {
    const { intro, steps, reviews, subjects, teachers, loading } = this.state;
    const formatter = new Intl.NumberFormat('it-IT', {
      style: 'currency',
      currency: 'VND',
    });
    return (
      <LayoutHeaderFooter>
        <Layout className="home">
          <Row>
            <Carousel autoplay={true} effect="fade" dotPosition='top'>
              {intro.map((el) => (<div className="mySlide" key={el.id}>
                <img className="mPicture" style={{ backgroundSize: 'cover', width: '100%' }} src={el.src} alt=""></img>
                <div className={`mText${el.id}`}>
                  <h3 style={{ color: 'white' }}>{el.title}</h3>
                  <h5 style={{ color: 'white', fontStyle: 'italic' }}>{el.description}</h5>
                </div>
              </div>))}
            </Carousel>
          </Row>
          <Divider orientation="left" style={{ fontSize: 25 }}>Các môn học nổi bật</Divider>
          <Row style={{ marginTop: 40 }}>
            {loading && <Skeleton rows={4}></Skeleton>}
            {!loading && subjects.map((el) => (
              <Col key={el._id} span={3} offset={1} style={{ display: 'flex', alignItems: 'center', flexDirection: 'column', marginBottom: 20, width: '20%', height: '40%', minWidth: 250 }}>
                <Card
                  hoverable
                  style={{ marginRight: '4%', textOverflow: 'ellipsis', width: '100%', height: '100%' }}
                  // https://i.ytimg.com/vi/Kp2bYWRQylk/maxresdefault.jpg
                  cover={<div><img alt={el.name} src={'https://i.ytimg.com/vi/Kp2bYWRQylk/maxresdefault.jpg'} style={{ width: '100%', height: 200, border: '2px solid orange' }} />
                  </div>}
                  onClick={() => { this.handleClickSubjects(el._id); }}
                >
                  <Card.Meta title={<div style={{ color: 'red' }}><Icon style={{ marginRight: 20 }} type="star"></Icon>TOP RATED</div>}
                    description={<p style={{ color: 'black', fontSize: 20, fontWeight: 'bold' }}>{el.name}</p>}/>
                </Card>
              </Col>
            ))}
          </Row>
          <Row style={{ textAlign: 'center', width: '100%', marginTop: 40 }}>Không thấy môn học bạn đang tìm kiếm?<a href="/teacher">{' Xem tất cả môn học'}</a></Row>
          <Divider orientation="left" style={{ fontSize: 25 }}> Các giáo viên xuất sắc</Divider>
          <Row style={{ marginTop: 40, marginLeft: 10, marginRight: 10 }}>
            {loading && <Skeleton rows={4}></Skeleton>}
            {!loading && teachers.map((el) => (
              <Col key={el.index} span={6} offset={1} style={{ display: 'flex', alignItems: 'center', flexDirection: 'column', marginBottom: 20, width: '25%', height: '40%', minWidth: 500 }}>
                {loading ? <Card style={{ marginRight: '4%', textOverflow: 'ellipsis', width: '100%', height: '100%' }}><Skeleton avatar paragraph={{ rows: 4 }} /></Card>
                  : <Card hoverable className="mCard"
                    style={{ marginRight: '4%', textOverflow: 'ellipsis', width: '100%', height: '100%' }}
                    title={<div style={{ color: 'green' }}><Icon style={{ marginRight: 20 }} type="star"></Icon>TOP RATED</div>}
                    actions={[<Icon type="info-circle" key="setting" />]}
                    onClick={() => { this.handleClickTeacher(el._id); }}
                  >
                    <Card.Meta
                      avatar={<Avatar size="large" src={url_backend + el.teacher.imageURL} />}
                      title={el.teacher.fullname}
                      description={<div><Rate value={(el.rate.reduce((total, items) => (total += items), 0) / el.rate.length)}></Rate>
                      </div>}
                    />
                    <Row style={{ margin: 10 }}>
                      <Col span={8} >
                        <Icon type="home" />
                        <p style={{ marginLeft: 10, color: 'blue', display: 'inline' }} >{el.teacher.location.city}</p>
                      </Col>
                      <Col span={8}>
                        <Icon type="phone" />
                        <p style={{ marginLeft: 10, color: 'blue', display: 'inline' }} >{el.teacher.phone}</p>
                      </Col>
                      <Col span={8}>
                        <Icon type="dollar" />
                        <p style={{ marginLeft: 10, color: 'red', display: 'inline' }} >{`${formatter.format(el.teacher.money)}`}</p>
                      </Col>
                    </Row>
                    <Divider></Divider>
                    {el.skill.map((el1) => (<Tag key={el1._id} color="cyan">{el1.name}</Tag>))}
                  </Card>}
              </Col>
            ))}
          </Row >
          <Row style={{ textAlign: 'center', width: '100%', marginTop: 40 }}>Tìm kiếm thêm các giáo viên?<a href="/teacher">{' Xem tất cả các giáo viên'}</a></Row>

          <Divider orientation="left" style={{ fontSize: 25 }}> Chúng tôi hoạt động như thế nào?</Divider>

          <Row style={{ marginTop: 40, marginLeft: 20, marginRight: 20 }}>
            <Steps onChange={this.onChange}>
              {steps.map((el) => (
                <Steps.Step status="process" key={el.id} title={el.title} description={<div style={{ textAlign: 'center' }}>
                  <Avatar size={100} src={el.src}></Avatar>
                  <h3>{el.description}</h3>
                </div>} />
              ))}
            </Steps>
          </Row>
          <Divider orientation="left" style={{ fontSize: 25, marginTop: 40 }}> Cảm nhận từ các học viên </Divider>
          <Row style={{ marginTop: 40, background: '#585656', height: 600, overflow: 'hidden' }}>
            <Carousel autoplay={true} effect="fade" dotPosition="top">
              {reviews.map((el) => (<div className="mySlide" key={el.id}>
                <img className="mPicture" style={{ backgroundSize: 'cover', width: '100%', opacity: 0.4 }} src={el.background} alt=""></img>
                <div className={'mText2'}>
                  <Avatar size={200} src={el.src}></Avatar>
                  <h3 style={{ color: 'white' }}>{`${el.name} - ${el.education}`}</h3>
                  <h5 style={{ color: 'white', fontStyle: 'italic' }}>{`"${el.description}"`}</h5>
                  <Rate value={el.rate} disabled></Rate>
                </div>
              </div>))}
            </Carousel>
          </Row>
        </Layout>

      </LayoutHeaderFooter>
    );
  }
}
export default withRouter(withApollo(Home));
