
import React, { useState, useEffect } from 'react';
import {
  notification,
  Button, Icon,
  Form, Spin,
} from 'antd';
import { useMutation, useQuery } from '@apollo/react-hooks';
import { connect } from 'react-redux';
import { gql } from 'apollo-boost';
import Skill from './Skill';
import Education from './education';
import Tags from './tags';
import OverView from './overview';


const WRITE_POST = gql`
  mutation WritePost( $input: PostInput!){
    writePost(input: $input)
}
`;
const TAGS = gql`
 {
  tags{
    _id
    name
    isAproved
  }
  }
`;
const MY_POST = gql`
{
    myPost{
    _id
   title
    overview
    hourlyRate
    skill{
      name
      isAproved
    }
    education{
      name
      timeStart
      timeEnd
      degree
      
      description
    }
    employeeHistory{
      title
      timeStart
      workPlace
      timeEnd
      description
    }
    rate
  }
  }
`;
const Index = (props) => {
  const { user } = props;
  const [skill, setSkill] = useState([]);
  const [title, setTitle] = useState('Tiêu đề của bạn');
  const [overview, setOverview] = useState('Mô tả chi tiết ');
  const [hourlyRate, setHourlyRate] = useState('0');
  const [loading, setLoading] = useState(true);
  const [writePost] = useMutation(WRITE_POST);
  const { data: dataTags } = useQuery(TAGS, {
    fetchPolicy: 'network-only',
  });
  const [employeeHistory, setEmployeeHistory] = useState([]);
  const [education, setEducation] = useState([]);
  const { data: dataPost } = useQuery(MY_POST, {
    fetchPolicy: 'network-only',
  });
  useEffect(() => {
    if (dataTags && dataPost) {
      if (dataPost.myPost !== null) {
        if (dataPost.myPost.education) {
          const arr = [];
          dataPost.myPost.education.forEach((ele) => {
            arr.push({
              degree: ele.degree,
              description: ele.description,
              name: ele.name,
              timeEnd: ele.timeEnd,
              timeStart: ele.timeStart,
            });
          });
          setEducation(arr);
        }
        if (dataPost.myPost.employeeHistory) {
          const arr = [];
          dataPost.myPost.employeeHistory.forEach((ele) => {
            arr.push({
              workPlace: ele.workPlace,
              title: ele.title,
              name: ele.name,
              timeEnd: ele.timeEnd,
              timeStart: ele.timeStart,
            });
          });
          setEmployeeHistory(arr);
        }
        if (dataPost.myPost.hourlyRate) {
          setHourlyRate(dataPost.myPost.hourlyRate);
        }
        if (dataPost.myPost.title) {
          setTitle(dataPost.myPost.title);
        }
        if (dataPost.myPost.overview) {
          setOverview(dataPost.myPost.overview);
        }
        const arr = [];
        if (dataPost.myPost.skill) {
          dataPost.myPost.skill.forEach((ele) => {
            arr.push(ele.name);
          });
          setSkill(arr);
        }
      }
      setLoading(false);
    }
  }, [dataTags, dataPost]);
  const handleFinish = () => {
    const arr = [];
    skill.forEach((ele) => {
      arr.push({ name: ele });
    });
    writePost({
      variables: {
        input: {
          title,
          overview,
          hourlyRate,
          skill: arr,
          education,
          employeeHistory,
        },
      },
    });

    notification.success({
      message: 'Update success',
    });
  };
  const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;
  return (

    <div>
      {!loading ? (
        <div>
          <OverView user={user} title={title} setTitle={setTitle} overview={overview}
            setOverview={setOverview} hourlyRate={hourlyRate} setHourlyRate={setHourlyRate} />
          <Tags dataTags={dataTags.tags} skill={skill} setSkill={setSkill} />
          <Skill employeeHistory={employeeHistory} setEmployeeHistory={setEmployeeHistory} />
          <Education education={education} setEducation={setEducation} />
          <div className="parent" style={{ width: 'auto', margin: '5px' }}>
            <div className="child">
              <div> <Button type="danger" onClick={handleFinish} >Hoàn tất</Button></div>
            </div>
          </div>
        </div>
      ) : (
          <div style={{ margin: '10px' }}>
            <Spin indicator={antIcon} ></Spin>
          </div>
          // eslint-disable-next-line indent
        )

      }
    </div >
  );
};

const mapStateToProps = (state) => ({
  user: state.user,
});
const App = Form.create()(Index);

export default connect(mapStateToProps, null)(App);
