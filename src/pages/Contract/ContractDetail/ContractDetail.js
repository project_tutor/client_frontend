import React, { useState } from 'react';
import {
  Layout, Row, Col, Avatar, Icon, Input,
  Divider, Timeline, Form, Button, InputNumber, notification,
  Radio,
} from 'antd';
import { connect } from 'react-redux';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

import FormFinish from './FormFinish/FormFinish';
import FormComplaint from './FormComplaint/FormComplaint';
import * as actions from '../../../tools/redux/actions';

const url_backend = process.env.REACT_APP_URL_BACKEND_API ? 'https://tutor-client-backend.herokuapp.com' : 'http://localhost:3001';
const ButtonGroup = Button.Group;

const TEACHER_AGREE_CONTRACT = gql`
  mutation teacherAgreeContract($contractID: ID!){
  teacherAgreeContract(contractID: $contractID)
}
`;

const TEACHER_DISAGREE_CONTRACT = gql`
  mutation teacherDisagreeContract($contractID: ID!){
  teacherDisagreeContract(contractID: $contractID)
}
`;

const STUDENT_PAY_CONTRACT = gql`
  mutation studentPayContract($contractID: ID!){
    studentPayContract(contractID: $contractID)
}
`;

function ContractDetail(props) {
  const { contractData, refetchDataContract, setVisibaleModal, user, handleSaveUser } = props;
  const { _id, teacher, student, title, description, skill, feeHourly, timeStudy, state } = contractData;
  const { fullname, location, phone, imageURL, email } = teacher;
  const { getFieldDecorator } = props.form;

  const [radioChecked, setRadioChecked] = useState('finish');
  const [teacherAgreeContract] = useMutation(TEACHER_AGREE_CONTRACT);
  const [teacherDisagreeContract] = useMutation(TEACHER_DISAGREE_CONTRACT);
  const [studentPayContract] = useMutation(STUDENT_PAY_CONTRACT);

  return (
    <Layout className="Detail-profile">
      <Form>
        <Row style={{ display: 'block' }}>
          <Form.Item label={'Tên hợp đồng:'}>
            {getFieldDecorator('title', {
              initialValue: title,
              rules: [
                {
                  required: true,
                  message: 'Tên hợp đồng không được bỏ trống!',
                },
              ],
            })(<Input readOnly />)}
          </Form.Item>
        </Row>
        <Divider style={{}} orientation="center">Thông tin</Divider>
        <Row style={{ background: 'white', overflow: 'hidden', display: 'flex', flexDirection: 'row' }}>
          <Col span={12} >
            <Row ><h1 style={{ color: 'red' }}> Giáo viên</h1></Row>
            <Row style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
              <Avatar src={url_backend + imageURL} size={150}></Avatar>
              <h1 style={{ marginTop: 5, color: 'green' }}>{fullname}</h1>
            </Row>
            <Divider orientation="left">Liên hệ</Divider>

            <Row style={{}}>
              <div>
                <Icon type="home" />
                <div style={{ marginLeft: 10, padding: 'unset', display: 'inline' }} >{`${location.address}, ${location.city}`} </div>
              </div>
              <div>
                <Icon type="phone" />
                <p style={{ marginLeft: 10, display: 'inline' }} >{phone}</p>
              </div>
              <div>
                <Icon type="mail" />
                <p style={{ marginLeft: 10, display: 'inline' }} >{email}</p>
              </div>
            </Row>

          </Col>

          <Timeline>
            <Timeline.Item style={{ height: '100%' }}></Timeline.Item>
            <Timeline.Item></Timeline.Item>
          </Timeline>
          <Col span={13}>
            <Row ><h1 style={{ color: 'red' }}> Học sinh</h1></Row>

            <Row style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
              <Avatar src={url_backend + student.imageURL} size={150}></Avatar>
              <h1 style={{ marginTop: 5, color: 'green' }}>{student.fullname}</h1>
            </Row>
            <Divider orientation="left">Liên hệ</Divider>

            <Row style={{}}>
              <div>
                <Icon type="home" />
                <div style={{ marginLeft: 10, padding: 'unset', display: 'inline' }} >{`${student.location.address}, ${student.location.city}`} </div>
              </div>
              <div>
                <Icon type="phone" />
                <p style={{ marginLeft: 10, display: 'inline' }} >{student.phone}</p>
              </div>
              <div>
                <Icon type="mail" />
                <p style={{ marginLeft: 10, display: 'inline' }} >{`${student.email}$/hrs`}</p>
              </div>
            </Row>
          </Col>
        </Row>
        <Divider style={{ marginTop: 50 }} orientation="center">Mô tả</Divider>
        <Row style={{ marginTop: 20 }}>
          <h3><b>Mô tả hợp đồng: </b></h3>
          <Form.Item>
            {getFieldDecorator('description', {
              rules: [
                {
                  required: true,
                  message: 'Mô tả không được bỏ trống!',
                },
              ],
            })(
              <div style={{ width: '100%', display: 'flex', flexDirection: 'row' }}>
                <Input.TextArea rows={8} readOnly value={description} />
              </div>,
            )}
          </Form.Item>
        </Row>
        <Row style={{ marginTop: 20 }}>
          <h3><b>Chọn môn học: </b></h3>
          <Form.Item label={'Tên hợp đồng:'}>
            {getFieldDecorator('skill', {
              initialValue: skill.name,
              rules: [
                {
                  required: true,
                  message: 'Tên hợp đồng không được bỏ trống!',
                },
              ],
            })(<Input readOnly />)}
          </Form.Item>
        </Row>
        <Divider style={{ marginTop: 50 }} orientation="center">Thanh toán</Divider>
        <Row style={{ marginTop: 20 }}>
          <h3><b>Tiền thuê trên giờ(VNĐ/Giờ): </b></h3>
          <Form.Item>
            {getFieldDecorator('feeHourly', {
              initialValue: feeHourly,
            })(
              <InputNumber
                formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                parser={(value) => value.replace(/\$\s?|(,*)/g, '')}
                style={{ width: 100 }}
                min={1}
                className="mInputHourly"
                readOnly
              />,
            )}
          </Form.Item>
        </Row>
        <Row style={{ marginTop: 20 }}>
          <h3><b>Số Giờ Thuê(Giờ): </b></h3>
          <Form.Item>
            {getFieldDecorator('timeStudy', {
              initialValue: timeStudy,
            })(
              <InputNumber
                formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                parser={(value) => value.replace(/\$\s?|(,*)/g, '').replace('H', '')}
                min={1}
                readOnly
              />,
            )}
          </Form.Item>
        </Row>
        <Row>
          <h3 style={{ color: 'red' }}><b>{`Tổng số tiền : ${(timeStudy * feeHourly).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')} VNĐ`}</b></h3>
        </Row>
        <Divider></Divider>

        {/* TEACHER STATE === PENDING */}
        {user.role === 'TEACHER' && state === 'PENDING'
          && <Row style={{ margin: '0 20' }}>
            <ButtonGroup style={{ width: '100%' }}>
              <Button
                size='large'
                type='primary'
                style={{ width: '50%' }}
                onClick={() => {
                  teacherAgreeContract({ variables: { contractID: _id } }).then(() => {
                    refetchDataContract();
                    setVisibaleModal(false);
                    notification.success({
                      message: 'Bạn đã đồng ý hợp đồng!',
                    });
                  });
                }}
              >
                Đồng ý
             </Button>
              <Button
                size='large'
                type='danger'
                style={{ width: '50%' }}
                onClick={() => {
                  teacherDisagreeContract({ variables: { contractID: _id } }).then(() => {
                    refetchDataContract();
                    setVisibaleModal(false);
                    notification.success({
                      message: 'Bạn đã từ chối hợp đồng!',
                    });
                  });
                }}
              >
                Từ chối
             </Button>
            </ButtonGroup>
          </Row>
        }

        {/* TEACHER STATE === WAITINGPAY */}
        {user.role === 'STUDENT' && state === 'WAITINGPAY'
          && <Row style={{ margin: '0 20' }}>
            <ButtonGroup style={{ width: '100%' }}>
              <Button
                size='large'
                type='primary'
                style={{ width: '100%' }}
                onClick={() => {
                  studentPayContract({ variables: { contractID: _id } })
                    .then(() => {
                      // Cap nhật lại tiền
                      handleSaveUser({ ...user, money: user.money - (feeHourly * timeStudy) });
                      refetchDataContract();
                      setVisibaleModal(false);
                      notification.success({
                        message: 'Bạn đã thanh toán hợp đồng!',
                      });
                    })
                    .catch(() => {
                      notification.error({
                        message: 'Bạn chưa đủ tiền!',
                      });
                    });
                }}
              >
                Thanh toán
             </Button>
            </ButtonGroup>
          </Row>
        }

        {/* TEACHER STATE === AGREE */}
        {user.role === 'STUDENT' && state === 'AGREE'
          && <Row style={{ margin: '0 20' }}>
            <div>
              <Radio.Group
                defaultValue={radioChecked}
                buttonStyle="solid"
                style={{ width: '100%' }}
                onChange={(e) => setRadioChecked(e.target.value)}
              >
                <Radio.Button value="finish" style={{ width: '50%', textAlign: 'center' }}>Hoàn tất</Radio.Button>
                <Radio.Button value="complant" style={{ width: '50%', textAlign: 'center', background: `${radioChecked === 'complant' ? 'red' : '#FFFFFF'}`, borderColor: `${radioChecked === 'complant' ? 'red' : '#FFFFFF'}` }} > Khiếu nại</Radio.Button>
              </Radio.Group>
            </div>

            {radioChecked === 'finish'
              ? <FormFinish contractID={_id} refetchDataContract={refetchDataContract} setVisibaleModal={setVisibaleModal} />
              : <FormComplaint contractID={_id} refetchDataContract={refetchDataContract} setVisibaleModal={setVisibaleModal} />
            }
          </Row>
        }
      </Form>
    </Layout >
  );
}

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
  handleSaveUser: (user) => {
    dispatch(actions.saveUser(user));
  },
});

export default Form.create({ name: 'ContractDetail' })(connect(mapStateToProps, mapDispatchToProps)(ContractDetail));
