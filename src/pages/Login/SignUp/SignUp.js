/* eslint-disable no-param-reassign */
import React from 'react';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';
import {
  Form, Input, Button, Icon, Radio, Modal,
} from 'antd';

const { confirm } = Modal;

class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isFacebookState: false,
      role: 'STUDENT',
      confirmDirty: false,

    };

    this.handleRegister = this.handleRegister.bind(this);
    this.handleLoginByFacebook = this.handleLoginByFacebook.bind(this);
    this.handleLoginByGoogle = this.handleLoginByGoogle.bind(this);

    this.compareToFirstPassword = this.compareToFirstPassword.bind(this);
    this.validateToNextPassword = this.validateToNextPassword.bind(this);
    this.OnChangeRole = this.OnChangeRole.bind(this);
    this.handleConfirmBlur = this.handleConfirmBlur.bind(this);
  }

  // componentWillReceiveProps() {
  //   if (!this.props.goToRegister && !this.props.loged) {
  //     this.props.form.setFieldsValue({
  //       email: '',
  //       fullname: '',
  //       password: '',
  //       confirm: '',
  //     });
  //   }
  // }

  handleRegister(e) {
    const { handleRegister } = this.props;
    const { validateFields } = this.props.form;
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        values.isSocial = false;
        values.imageURL = '';
        handleRegister(values, this.state.role);
      }
    });
  }

  handleConfirmBlur(e) {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

  handleLoginByFacebook(response) {
    if (!response.status) {
      const { role } = this.state;
      confirm({
        title: `Bạn muốn đăng ký tài khoản ${role === 'STUDENT' ? '"HỌC SINH"' : '"GIÁO VIÊN"'} bằng facebook?`,
        content: `Loại tài khoản bạn đang chọn là tài khoảng dành cho ${role === 'STUDENT' ? 'học sinh' : 'giáo viên'}`,
        onOk: () => {
          const { handleRegister } = this.props;
          handleRegister({ email: response.email, password: response.id, fullname: response.name, imageURL: response.picture.data.url, isSocial: true }, role);
        },
      });
    }
  }

  handleLoginByGoogle(response) {
    const { profileObj } = response;
    if (profileObj) {
      const { role } = this.state;
      confirm({
        title: `Bạn muốn đăng ký tài khoản ${role === 'STUDENT' ? '"HỌC SINH"' : '"GIÁO VIÊN"'} bằng facebook?`,
        content: `Loại tài khoản bạn đang chọn là tài khoảng dành cho ${role === 'STUDENT' ? 'học sinh' : 'giáo viên'}`,
        onOk: () => {
          const { handleRegister } = this.props;
          handleRegister({ email: profileObj.email, password: profileObj.googleId, fullname: profileObj.name, imageURL: profileObj.imageUrl, isSocial: true }, role);
        },
      });
    }
  }

  OnChangeRole(e) {
    if (e.target.value !== this.state.role) {
      this.setState({
        role: e.target.value,
      });
    }
  }

  compareToFirstPassword(rule, value, callback) {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('password')) {
      callback('Mật khẩu không trùng khớp!');
    } else {
      callback();
    }
  }

  validateToNextPassword(rule, value, callback) {
    const { form } = this.props;
    if (value && value.length < 6) {
      callback('Mật khẩu phải có ít nhất 6 ký tự');
    }
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { loading } = this.props;

    return (
      <div className="form-container sign-up-container">
        <Form onSubmit={this.handleRegister} className="login-form">
          <h1>Đăng Ký</h1>
          <Form.Item style={{ marginBottom: 0 }}>
            <div className="social-container">
              <FacebookLogin
                appId="572752733493843" // APP ID NOT CREATED YET
                fields="name,email,picture"
                callback={this.handleLoginByFacebook}
                cssClass="social-button"
                icon="fa-facebook-f"
                textButton=""
                autoLoad={false}
                isDisabled={loading}
              />
              <GoogleLogin
                clientId="264218692025-aqmc5agm1fmun0d793hl39ctugaa8jgu.apps.googleusercontent.com"
                render={(renderProps) => (
                  <Button type="danger" ghost className="btn" onClick={renderProps.onClick} disabled={renderProps.disabled || loading}>
                    <Icon type="google" />
                  </Button>
                )}
                onSuccess={this.handleLoginByGoogle}
              />
            </div>
            <span>hoặc tạo mới tài khoản của bạn để đăng ký</span>
          </Form.Item>
          <div className="mInput social-container" >
            <Radio.Group disabled={loading} onChange={this.OnChangeRole} defaultValue={this.state.role} buttonStyle="solid">
              <Radio.Button value="STUDENT">Học Sinh</Radio.Button>
              <Radio.Button value="TEACHER">Giảng viên</Radio.Button>
            </Radio.Group>
          </div>
          <Form.Item className="mInput">
            {getFieldDecorator('email', {
              rules: [
                {
                  required: true,
                  message: 'Email không được bỏ trống!',
                },
                {
                  type: 'email',
                  message: 'Không đúng định dạng E-Mail!',
                },
              ],
            })(<Input disabled={loading} placeholder="Nhập vào Email" />)}
          </Form.Item>
          <Form.Item className="mInput">
            {getFieldDecorator('fullname', {
              rules: [
                {
                  required: true,
                  message: 'Họ và tên không được bỏ trống!',
                },
              ],
            })(<Input disabled={loading} placeholder="Nhập vào Họ và tên" />)}
          </Form.Item>
          <Form.Item className="mInput" hasFeedback>
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: 'Mật khẩu không được bỏ trống!',
                },
                {
                  validator: this.validateToNextPassword,
                },
              ],
            })(<Input.Password disabled={loading} type="password" placeholder="Nhập vào mật khẩu" onBlur={this.handleConfirmBlur} />)}
          </Form.Item>
          <Form.Item className="mInput" hasFeedback >
            {getFieldDecorator('confirm', {
              rules: [
                {
                  required: true,
                  message: 'Xin vui lòng xác nhận mật khẩu!',
                },
                {
                  validator: this.compareToFirstPassword,
                },
              ],
            })(<Input disabled={loading} onBlur={this.handleConfirmBlur} type="password" placeholder="Xác nhận mật khẩu" />)}
          </Form.Item>
          <Button style={{ width: '70%' }} type="primary" htmlType="submit" className="login-form-button" loading={loading}>
            Đăng ký
                    </Button>
        </Form>
      </div>
    );
  }
}
const Register = Form.create({ name: 'Register' })(SignUp);
export default Register;
