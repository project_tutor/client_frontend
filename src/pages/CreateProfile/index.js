/* eslint-disable eol-last */
/* eslint-disable lines-between-class-members */
import React, { useState } from 'react';

import 'antd/dist/antd.css';
import {
  Card, Row, Col,
} from 'antd';
import { useMutation } from '@apollo/react-hooks';
import { connect } from 'react-redux';
import { gql } from 'apollo-boost';
import Header from './Components/header';
import BodyCard from './Components/body';
import Education from './Components/Education';
import History from './Components/HistoryExp';
import SubmitFinall from './Components/SubmitForm';
import LayoutHeaderFooter from '../../components/LayoutDefault/HeaderFooter/HeaderFooter';
import './Components/index.css';

const EDIT_USER = gql`
  mutation EditUser($userID: ID!, $input: UserInput!){
  editUser(userID: $userID, input: $input)
}
`;

const SubmitForm = (props) => {
  const { user } = props;
  const [number, setNumber] = useState(0);
  const [education, setEducation] = useState([]);
  const [imgURL, setImgURL] = useState('');
  const [fullname, setFullname] = useState('');
  const [location, setLocation] = useState({});
  const [phone, setPhone] = useState('');
  const [level, setLevel] = useState('');
  const [history, setHistory] = useState([]);
  const [skill, setSkill] = useState([]);
  const [editUser] = useMutation(EDIT_USER);
  const arr_thongtin = ['Thông tin', 'Học Vấn', 'Kinh nghiệm'];
  const ComformData = () => {
    editUser({
      variables: {
        // eslint-disable-next-line no-underscore-dangle
        userID: user._id,
        input: {
          fullname,
          phone,
          location,
          education,
          employeeHistory: history,
        },
      },
    });
  };

  const BodyProfice = () => {
    if (number === 0) {
      return (
        <BodyCard location={location} setLocation={setLocation}
          imgURL={imgURL} setImgURL={setImgURL}
          setFullname={setFullname}
          fullname={fullname}
          number={number} setNumber={setNumber}
          phone={phone} setPhone={setPhone}
          level={level} setLevel={setLevel}
        ></BodyCard>
      );
    } if (number === 1) {
      return <Education education={education} setEducation={setEducation}
        number={number} setNumber={setNumber}
      />;
    } if (number === 2) {
      return <History history={history} setHistory={setHistory}
        number={number} setNumber={setNumber}
        ComformData={ComformData}
      />;
    }
    return <SubmitFinall skill={skill}
      setSkill={setSkill} education={education}
      history={history} fullname={fullname}
      location={location}
    ></SubmitFinall>;
  };
  return (
    <LayoutHeaderFooter>
      <Row>
        <Header vitri={number} />
        <div style={{ marginBottom: '5px' }}></div>
        <Col span={12} offset={6}>
          <Card title={arr_thongtin[number]} bordered={true} style={{ width: 'auto' }} >
            {BodyProfice()}
            <div style={{ marginBottom: '5px' }}></div>
          </Card>
        </Col>
      </Row>
    </LayoutHeaderFooter >
  );
};

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps, null)(SubmitForm);
