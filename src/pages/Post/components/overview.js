/* eslint-disable no-underscore-dangle */
/* eslint-disable indent */
import React, { useState, useEffect } from 'react';
import {
    Input, notification,
    Skeleton, Avatar, Card, Button,
    Drawer, Form, Row, Col,
} from 'antd';

const { Meta } = Card;
const url_backend = process.env.REACT_APP_URL_BACKEND_API ? 'https://tutor-client-backend.herokuapp.com' : 'http://localhost:3001';
const OverView = (props) => {
    const [visible, setVisible] = useState(false);
    const [loading, setLoading] = useState(true);
    const { hourlyRate,
        setHourlyRate,
        title, setTitle,
        overview, setOverview, user,
    } = props;
    const showDrawer = () => {
        setVisible(true);
    };
    useEffect(() => {
        if (user) {
            setLoading(false);
        }
    }, [user]);
    const onClose = () => {
        setVisible(false);
    };
    const handleSubmit = (events) => {
        events.preventDefault();
        props.form.validateFields((err, values) => {
            if (!err) {
                onClose();
                setTitle(values.title);
                setOverview(values.descrip);
                setHourlyRate(values.hocphi);
            }
        });
        notification.success({
            message: 'Update success',
        });
    };
    const { getFieldDecorator } = props.form;


    return (
        <div>

            <div>
                <Card
                    title="Thông tin cá nhân"
                    extra={<Button onClick={showDrawer} type="dashed" size="small" shape="circle" icon="edit" />}
                    style={{ width: 'auto' }}
                >
                    <Skeleton avatar loading={loading} active
                    >
                        <Meta
                            avatar={
                                <Avatar src={url_backend + user.imageURL} />
                            }
                            title={user.fullname}
                            description={`${user.location.address ? user.location.address : ''}   ${user.location.city ? user.location.city : ''} `}
                        >
                        </Meta>
                    </Skeleton>

                    <p style={{ marginTop: '10px', fontWeight: 'bold', wordWrap: 'break-word' }}><label style={{ marginRight: '10px' }}>{title}</label>
                    </p>
                    <div style={{ width: 'auto', wordWrap: 'break-word' }} >
                        <p><label style={{ marginRight: '10px' }}></label>{overview}</p>
                    </div>
                    <p style={{ marginTop: '10px', fontWeight: 'bold', wordWrap: 'break-word' }}><label style={{ marginRight: '10px' }}>Học phí: {hourlyRate} VND</label>
                    </p>
                </Card>
                <Drawer
                    title="Thông tin chi tiết"
                    width={720}
                    onClose={onClose}
                    visible={visible}
                    bodyStyle={{ paddingBottom: 80 }}
                >
                    <Form layout="vertical" hideRequiredMark>
                        <Row gutter={16}>
                            <Col >
                                <Form.Item label="Tiêu đề">
                                    {getFieldDecorator('title', {
                                        initialValue: title,
                                        rules: [{ required: true, message: 'Vui lòng nhập chức vụ' }],
                                    })(
                                        <Input maxLength='40' placeholder="Vui lòng nhập chức vụ" />,
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col >
                                <Form.Item label="Mô Tả chi tiết">
                                    {getFieldDecorator('descrip', {
                                        initialValue: overview,
                                        rules: [{ required: true, message: 'Vui lòng giới thiệu bản thân' }],
                                    })(
                                        <Input.TextArea rows={4} placeholder="please enter url description" />,
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col >
                                <Form.Item label="Học phí/H">
                                    {getFieldDecorator('hocphi', {
                                        initialValue: hourlyRate,
                                        rules: [{ required: true, message: 'Vui lòng giới thiệu bản thân' }],
                                    })(
                                        <Input type="text" maxLength='40' placeholder="Học phí trên giờ" />,
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                    <div
                        style={{
                            position: 'absolute',
                            right: 0,
                            bottom: 0,
                            width: '100%',
                            borderTop: '1px solid #e9e9e9',
                            padding: '10px 16px',
                            background: '#fff',
                            textAlign: 'right',
                        }}
                    >
                        <Button onClick={onClose} style={{ marginRight: 8 }}>
                            Hủy
            </Button>
                        <Button onClick={handleSubmit} type="primary">
                            Đồng ý
            </Button>
                    </div>
                </Drawer>
            </div>
        </div >
    );
};


const App = Form.create()(OverView);

export default App;
