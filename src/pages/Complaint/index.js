import React, { useState } from 'react';
import {
  Table, Tag, Modal, Button, Input, Form, Row, Col, notification, Spin,
} from 'antd';
import { gql } from 'apollo-boost';
import { connect } from 'react-redux';
import { useQuery, useMutation } from '@apollo/react-hooks';
import HeaderFooterSilder from '../../components/LayoutDefault/HeaderFooterSilder/HeaderFooterSilder';

const GET_MY_COMPLAINT = gql`
  query myComplaints {
    myComplaints{
    contract{
      state
      title
      _id
    }
    _id
    state
    studentMessage
    teacherMessage
    whoAccepted
  }
}
`;
const TEACHER_WRITE_COMPLAINT = gql`
 mutation TeacherWriteMessageInComplaint($complaintID:ID!,$teacherMessage:String!){
  teacherWriteMessageInComplaint(complaintID:$complaintID,teacherMessage:$teacherMessage)
 }
`;
const STUDENT_WRITE_COMPLAINT = gql`
 mutation StudentUPdateComplaint($complaintID:ID!,$studentMessage:String!){
  studentUPdateComplaint(complaintID:$complaintID,studentMessage:$studentMessage)
 }
`;


function getColorTag(text) {
  switch (text) {
    case 'PENDING':
      return 'GREEN';
    case 'AGREE':
      return 'LIME';
    case 'DISAGREE':
      return 'RED';
    case 'FINISH':
      return 'BLUE';
    case 'PROCESSED':
      return 'RED';
    default:
      break;
  }
}

const Index = (props) => {
  const { user } = props;
  const { getFieldDecorator } = props.form;
  const { loading: loadingDataComplaints, data: dataComplaints, refetch: reDataComplaints } = useQuery(GET_MY_COMPLAINT, { fetchPolicy: 'network-only' });
  const [teacherWriteMessageInComplaint] = useMutation(TEACHER_WRITE_COMPLAINT);
  const [studentUPdateComplaint] = useMutation(STUDENT_WRITE_COMPLAINT);
  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [idx, setIdx] = useState({});

  if (loadingDataComplaints) return <HeaderFooterSilder><Spin /></HeaderFooterSilder>;

  const showModal = () => {
    setVisible(true);
  };
  const handleCancel = async () => {
    console.log('Clicked cancel button');
    setIdx(null);
    setVisible(false);
  };
  const handleOk = (events) => {
    events.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        setConfirmLoading(true);
        if (user.role === 'STUDENT') {
          studentUPdateComplaint({
            variables: {
              complaintID: idx._id,
              studentMessage: values.phanhoi,
            },
          }).then(() => {
            setVisible(false);
            setConfirmLoading(false);
            setIdx(null);
            reDataComplaints();
            notification.success({
              message: 'Phản hồi thành công',
            });
          }).catch(() => {
            setVisible(false);
            setConfirmLoading(false);
            setIdx(null);
            reDataComplaints();
            notification.error({
              message: 'Phản hồi Thất bại',
            });
          });
        } else {
          teacherWriteMessageInComplaint({
            variables: {
              complaintID: idx._id,
              teacherMessage: values.phanhoi,
            },
          }).then(() => {
            setVisible(false);
            setConfirmLoading(false);
            setIdx(null);
            reDataComplaints();
            notification.success({
              message: 'Phản hồi thành công',
            });
          }).catch(() => {
            setVisible(false);
            setConfirmLoading(false);
            setIdx(null);
            reDataComplaints();
            notification.error({
              message: 'Phản hồi Thất bại',
            });
          });
        }
      } else {
        notification.error({
          message: 'Vui lòng nhập thông tin',
        });
      }
    });
  };
  const columns = [
    {
      title: 'Trạng thái',
      dataIndex: 'state',
      width: 150,
      // eslint-disable-next-line arrow-body-style
      render: (text) => (
        <div style={{ textAlign: 'center', fontWeight: 'bold' }}>
          <Tag color={getColorTag(text)} key={text}>
            {text.toUpperCase()}
          </Tag>
        </div>
      ),
    },
    {
      title: 'Tên hợp đồng',
      dataIndex: 'contract.title',
    },
    {
      title: 'Nội dung khiếu nại',
      dataIndex: 'studentMessage',
    },
    {
      title: 'Phản hồi từ giáo viên',
      dataIndex: 'teacherMessage',
    },
    {
      title: 'Tác vụ',
      key: 'action',
      // fixed: 'right',
      width: 120,
      render: (item) => (
        <span>
          {item.state === 'PENDING'
            && <Button type="link" size="small"
              onClick={() => {
                const temp = item;
                temp.IsCheck = true;
                setIdx(temp);
                showModal();
              }} >
              Phản hồi
            </Button>
          }
        </span>

      ),
    },
  ];
  return (
    <HeaderFooterSilder>
      <div>
        <Table
          columns={columns}
          dataSource={dataComplaints ? dataComplaints.myComplaints : null}
          bordered
          rowKey='_id'
          title={() => (
            <h3>Danh sách khiếu nại</h3>
          )}
          scroll={{ y: 500 }}
        />
      </div>
      <Modal
        title="Phản hồi"
        visible={visible}
        onOk={handleOk}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
      >
        <Form layout="vertical" hideRequiredMark>
          <Row gutter={16}>
            <Col span={24}>
              <Form.Item >
                {getFieldDecorator('phanhoi', {
                  rules: [
                    {
                      required: true,
                      message: 'Nhập thông tin muốn phản hồi',
                    },
                  ],
                })(<Input.TextArea rows={4} maxLength="600" placeholder="Nhập thông tin muốn phản hồi" />)}
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Modal>
    </HeaderFooterSilder>
  );
};
const mapStateToProps = (state) => ({
  user: state.user,
});
const App = Form.create()(Index);
export default connect(mapStateToProps, null)(App);
