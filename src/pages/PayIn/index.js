/* eslint-disable comma-dangle */
/* eslint-disable quotes */
/* eslint-disable quote-props */
import React, { useState } from 'react';
import {
  Input,
  Card, Form, Row, Col, InputNumber, notification,
} from 'antd';
import { useMutation } from '@apollo/react-hooks';
import { connect } from 'react-redux';
import { gql } from 'apollo-boost';
import { PayPalButton } from "react-paypal-button-v2";
import { useHistory } from 'react-router-dom';
import * as types from '../../tools/redux/actions';

import HeaderFooterSilder from '../../components/LayoutDefault/HeaderFooterSilder/HeaderFooterSilder';


const EDIT_MONEY = gql`
  mutation EditMoney($money:String!){
    editMoney(money:$money)
  }
`;
const PayIn = (props) => {
  const { getFieldDecorator } = props.form;
  const { user, saveUser } = props;
  const [money, setMoney] = useState(10);
  const [editMoney] = useMutation(EDIT_MONEY);
  const history = useHistory();

  if (user.role === 'TEACHER') {
    history.push('/');
  }

  function onChange(value) {
    console.log('changed', value);
    setMoney(value);

    if (value < 10) {
      setMoney(10);
    }
  }
  const handlesubmit = async () => {
    if (!user.money) {
      user.money = 0;
    }
    const a = money * 23000 + parseInt(user.money, 10);
    await editMoney({
      variables: {
        money: a.toString(),
      },
    });
    await saveUser({
      ...user,
      money: a,
    });
  };
  return <HeaderFooterSilder>
    <div >
      <div style={{ background: '#ECECEC' }}>
        <Card
          title="Nạp tiền vào tài khoản"
          width={720}
          bodyStyle={{ paddingBottom: 5 }}
        >
          <Form layout="vertical" style={{ marginBottom: '10px' }} hideRequiredMark>
            <Row gutter={16}>
              <Col >
                <Form.Item label="Họ tên ">
                  {getFieldDecorator('name', {
                    initialValue: user ? user.fullname : null,
                    rules: [{ required: true, message: 'Vui lòng nhập họ tên' }],
                  })(<Input readOnly="true" maxLength='40' placeholder="Nguyễn Văn A" />)}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={12}>
              <Col >
                <p>Số tiền:</p>
                <InputNumber
                  style={{ width: '100%' }}
                  value={money}
                  formatter={(value) => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                  parser={(value) => value.replace(/\$\s?|(,*)/g, '')}
                  onChange={onChange}
                />
              </Col>
            </Row>
          </Form>
          <p>Phương thức thanh toán:</p>
          <div style={{ backgroundColor: 'white', marginTop: 50 }}>
            <div style={{ margin: 'auto', width: '50%', backgroundColor: 'white' }}>
              <PayPalButton
                amount={money}
                onSuccess={(details, data) => {
                  handlesubmit();
                  notification.success({
                    message: 'Nạp tiền thành công ',
                  });
                  // OPTIONAL: Call your server to save the transaction
                  return fetch('/paypal-transaction-complete', {
                    method: 'post',
                    body: JSON.stringify({
                      orderId: data.orderID
                    })
                  });
                }}
                options={{
                  clientId: 'AWHWaA5nhE22vx7kFdSkpSm1S9-8YZrdgTmu7MaNo7MwBFrWmVOYrOZ9u4xI9y9FiC9vVVMc53qrpahT',
                }}
              />
            </div>
          </div>
        </Card>


      </div>

    </div>
  </HeaderFooterSilder >;
};
const mapStateToProps = (state) => ({
  user: state.user,
});
// eslint-disable-next-line arrow-body-style
const maptoProps = (dispatch) => {
  return {
    saveUser: (value) => dispatch(types.saveUser(value)),
  };
};
const App = Form.create()(PayIn);
export default connect(mapStateToProps, maptoProps)(App);
