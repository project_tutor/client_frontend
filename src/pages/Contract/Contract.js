import React, { useState } from 'react';
import { Table, Button, Icon, Tag, Modal, Spin } from 'antd';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { connect } from 'react-redux';

import HeaderFooterSilder from '../../components/LayoutDefault/HeaderFooterSilder/HeaderFooterSilder';
import ContractDetail from './ContractDetail/ContractDetail';
import './style.css';

const GET_MY_CONTRACTS = gql`
  query myContracts {
  myContracts {
    _id
    teacher {
      email
      fullname
      phone
      imageURL
      location {
        address
        city
      }
    }
    student {
      email
      fullname
      phone
      imageURL
      location {
        address
        city
      }
    }
    title
    description
    feeHourly
    timeStudy
    state
    skill {
      name
    }
  }
}
`;

// , refetch: refetchDataContract

function getColorTag(text) {
  switch (text) {
    case 'PENDING':
      return 'GRAY';
    case 'AGREE':
      return 'LIME';
    case 'DISAGREE':
      return 'RED';
    case 'FINISH':
      return 'BLUE';
    case 'COMPLAINING':
      return 'OLIVE';
    default:
      break;
  }
}

function Contract(props) {
  const { user } = props;
  const { loading: loadingDataContract, data: dataContract, refetch: refetchDataContract } = useQuery(GET_MY_CONTRACTS, { fetchPolicy: 'network-only' });
  const indexDoiTac = user.role === 'STUDENT' ? 'teacher' : 'student';
  const [contractSelected, setContractSelected] = useState(null);
  const [visibaleModal, setVisibaleModal] = useState(false);
  const columns = [
    {
      title: 'Trạng thái',
      dataIndex: 'state',
      width: 150,
      // eslint-disable-next-line arrow-body-style
      render: (text) => (
        <div style={{ textAlign: 'center', fontWeight: 'bold' }}>
          <Tag color={getColorTag(text)} key={text}>
            {text.toUpperCase()}
          </Tag>
        </div>
      ),
    },
    {
      title: 'Đối tác',
      dataIndex: `${indexDoiTac}.fullname`,
    },
    {
      title: 'Môn học',
      dataIndex: 'skill.name',
    },
    {
      title: 'Thời lượng',
      dataIndex: 'timeStudy',
    },
    {
      title: 'Giá/Giờ',
      dataIndex: 'feeHourly',
      render: (text) => (
        <>{`${text.replace(/\B(?=(\d{3})+(?!\d))/g, ',')} VND`}</>
      ),
    },
    // {
    //   title: 'Trạng thái',
    //   dataIndex: 'isAproved',
    //   // eslint-disable-next-line arrow-body-style
    //   render: (text) => (
    //     <div style={{ fontWeight: 'bold' }}>
    //       {text ? <span style={{ color: 'green' }}>Đã duyệt</span> : < span style={{ color: 'red' }}> Chưa duyệt</span >}
    //     </div >
    //   ),
    // },
    {
      title: 'Tắc vụ',
      key: 'action',
      // fixed: 'right',
      width: 100,
      render: (item) => (
        <>
          <Button type="primary" size="small" onClick={() => {
            setVisibaleModal(true);
            setContractSelected(item);
          }}>
            <Icon type="eye" />
          </Button>
        </>
      ),
    },
  ];

  if (loadingDataContract) return <HeaderFooterSilder><Spin /></HeaderFooterSilder>;

  return (
    <HeaderFooterSilder>
      <Table
        columns={columns}
        dataSource={dataContract ? dataContract.myContracts : null}
        bordered
        rowKey='_id'
        title={() => (
          <h3>Hợp đồng</h3>
        )}
        scroll={{ y: 450 }}
      />
      <Modal
        title="Chi tiết hợp đồng"
        visible={visibaleModal}
        width={'60%'}
        onOk={() => setVisibaleModal(false)}
        onCancel={() => setVisibaleModal(false)}
        footer={
          <div style={{ color: 'gray', textAlign: 'center' }}>(C) Bản quyền thuộc về UberTutor</div>
        }
      >
        <ContractDetail
          contractData={contractSelected}
          refetchDataContract={refetchDataContract}
          setVisibaleModal={setVisibaleModal}
        />
      </Modal>
    </HeaderFooterSilder>
  );
}

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps, null)(Contract);
