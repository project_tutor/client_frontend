import React from 'react';
import { Form, Input, Button, PageHeader, Spin, notification } from 'antd';
import { connect } from 'react-redux';
import { gql } from 'apollo-boost';
import { useQuery, useMutation } from '@apollo/react-hooks';

import HeaderFooterSilder from '../../components/LayoutDefault/HeaderFooterSilder/HeaderFooterSilder';

const CHANGE_PASSWORD = gql`
  mutation changePassword($oldPassword: String!, $newPassword: String!){
  changePassword(oldPassword: $oldPassword, newPassword: $newPassword)
}
`;

const USER_LOGIN = gql`
  query userLogin {
    userLogin {
      isSocial
    }
  }
`;

const Profile = (props) => {
  const { getFieldDecorator } = props.form;
  const { user } = props;
  const { loading, data: dataUser } = useQuery(USER_LOGIN, {
    fetchPolicy: 'network-only',
  });
  const [changePassword] = useMutation(CHANGE_PASSWORD);

  if (loading) {
    return (
      <HeaderFooterSilder>
        <PageHeader
          style={{
            border: '1px solid rgb(235, 237, 240)',
          }}
          title="Đổi mật khẩu"
        />
        <Spin></Spin>
      </HeaderFooterSilder>
    );
  }
  let isSocial = false;
  if (dataUser) {
    isSocial = dataUser.userLogin.isSocial;
  }

  // Submit Form
  const handleSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        changePassword({
          variables: {
            oldPassword: values.oldPassword,
            newPassword: values.password,
          },
        }).then(() => {
          props.form.resetFields();
          notification.success({
            message: 'Cập nhật thành công!',
          });
        }).catch((error) => {
          notification.error({
            message: error.message.substring(15),
          });
        });
      }
    });
  };

  const compareToFirstPassword = (rule, value, callback) => {
    const { form } = props;
    if (value && value !== form.getFieldValue('password')) {
      callback('Mật khẩu xác nhận chưa chính xác!');
    } else {
      callback();
    }
  };

  const validateToNextPassword = (rule, value, callback) => {
    // const { form } = props;
    // if (value && this.state.confirmDirty) {
    //   form.validateFields(['confirm'], { force: true });
    // }
    callback();
  };

  return (
    <HeaderFooterSilder>
      <PageHeader
        style={{
          border: '1px solid rgb(235, 237, 240)',
        }}
        title="Đổi mật khẩu"
      />
      {user && isSocial ? <div>Tài khoản này không hỗ trợ chức năng thay đổi mật khẩu </div> : <Form className="login-form" onSubmit={handleSubmit} style={{ width: '100%', padding: '20px' }}>
        <Form.Item id="fullname" label="Mật khẩu hiện tại" >
          {getFieldDecorator('oldPassword', {
            initialValue: '',
            rules: [{ required: true, message: 'Vui lòng nhập mật khẩu' }],
          })(
            <Input.Password />,
          )}
        </Form.Item>
        <Form.Item id="fullname" label="Mật khẩu mới" >
          {getFieldDecorator('password', {
            initialValue: '',
            rules: [
              {
                required: true, message: 'Vui lòng nhập mật khẩu mới',
              },
              {
                validator: validateToNextPassword,
              },
            ],
          })(
            <Input.Password />,
          )}
        </Form.Item>
        <Form.Item id="fullname" label="Xác nhận mất khẩu mới" >
          {getFieldDecorator('confirm', {
            initialValue: '',
            rules: [
              {
                required: true, message: 'Vui lòng nhập xác nhận mật khẩu mới',
              },
              {
                validator: compareToFirstPassword,
              },
            ],
          })(
            <Input.Password />,
          )}
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button">
            Cập nhật
            </Button>
        </Form.Item>
      </Form>}
    </HeaderFooterSilder>
  );
};

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(Profile);

const mapStateToProps = (state) => ({
  user: state.user,
});
export default connect(mapStateToProps, null)(WrappedNormalLoginForm);
